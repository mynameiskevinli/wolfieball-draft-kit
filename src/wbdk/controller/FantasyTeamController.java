/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import static wbdk.WBDK_PropertyType.REMOVE_ITEM_MESSAGE;
import wbdk.data.Player;
import wbdk.data.Pitcher;
import wbdk.data.Hitter;
import wbdk.data.PlayerHolder;
import wbdk.data.FantasyTeam;
import wbdk.gui.FantasyTeamDialog;
import wbdk.gui.WBDK_GUI;
import wbdk.gui.MessageDialog;
import wbdk.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.DataManager;
import javafx.scene.control.ComboBox;
import wbdk.data.TeamHolder;

/**
 *
 * @author Kevin
 */
public class FantasyTeamController {
    FantasyTeamDialog ftd;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public FantasyTeamController(Stage initPrimaryStage, TeamHolder tH, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        ftd = new FantasyTeamDialog(initPrimaryStage, tH, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    
    public void handleAddTeamRequest(WBDK_GUI gui) {
        DataManager dm = gui.getDataManager();
        TeamHolder tH = dm.getTeamHolder();
        ftd.showAddTeamDialog();
        
        // DID THE USER CONFIRM?
        if (ftd.wasCompleteSelected()) {
            // UPDATE THE FANTASY TEAM
            FantasyTeam team = new FantasyTeam();
            team.setTeamName(ftd.getTeam().getTeamName());
            team.setTeamOwner(ftd.getTeam().getTeamOwner());
            tH.addTeam(team);
            gui.getDisplayTeams().add(team);
            dm.getPlayerHolder().setEstimatedValues(tH);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditTeamRequest(WBDK_GUI gui, FantasyTeam teamToEdit) {
        DataManager dm = gui.getDataManager();
        TeamHolder th = dm.getTeamHolder();
        ftd.showEditTeamDialog(teamToEdit);
        
        // DID THE USER CONFIRM?
        if (ftd.wasCompleteSelected()) {
            // UPDATE THE FANTASY TEAM
            FantasyTeam team = ftd.getTeam();
            teamToEdit.setTeamName(team.getTeamName());
            teamToEdit.setTeamOwner(team.getTeamOwner());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleRemoveTeamRequest (WBDK_GUI gui, FantasyTeam teamToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            for (int i = 0; i < teamToRemove.getListOfActivePlayers().size(); i++) {
                gui.getDisplayPlayers().add(teamToRemove.getListOfActivePlayers().get(i));
                teamToRemove.getListOfActivePlayers().get(i).setFTeam("");
            }
            
            for (int i = 0; i < teamToRemove.getListOfTaxiPlayers().size(); i++) {
                gui.getDisplayPlayers().add(teamToRemove.getListOfActivePlayers().get(i));
                teamToRemove.getListOfTaxiPlayers().get(i).setFTeam("");
            }
            gui.getDisplayTeams().remove(teamToRemove);
            gui.getDraftedPlayers().removeAll(teamToRemove.getListOfActivePlayers());
            gui.getDraftedPlayers().removeAll(teamToRemove.getListOfTaxiPlayers());
            gui.getDataManager().getTeamHolder().removeTeam(teamToRemove);
        }
    }
}
