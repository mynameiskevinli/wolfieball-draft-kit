/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

//import static wbdk.WBDK_PropertyType.COURSE_SAVED_MESSAGE;
//import static wbdk.WBDK_PropertyType.NEW_COURSE_CREATED_MESSAGE;
//import static wbdk.WBDK_PropertyType.SAVE_UNSAVED_WORK_MESSAGE;
//import static wbdk.WBDK_StartupConstants.PATH_COURSES;
import wbdk.data.Player;
import wbdk.data.Player;
import wbdk.data.Player;
import wbdk.data.DataManager;
import wbdk.file.FileManager;
import wbdk.gui.WBDK_GUI;
import wbdk.gui.MessageDialog;
import wbdk.gui.YesNoCancelDialog;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import static wbdk.WBDK_PropertyType.DRAFT_LOADED_MESSAGE;
import static wbdk.WBDK_PropertyType.DRAFT_SAVED_MESSAGE;
import static wbdk.WBDK_PropertyType.NEW_DRAFT_CREATED_MESSAGE;
import static wbdk.WBDK_PropertyType.SAVE_UNSAVED_WORK_MESSAGE;
import static wbdk.WBDK_StartupConstants.PATH_DRAFTS;
import wbdk.data.Draft;
import wbdk.data.FantasyTeam;

/**
 *
 * @author Kevin
 */
public class FileController {
    
    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THIS GUY KNOWS HOW TO READ AND WRITE COURSE DATA
    private FileManager draftIO;

    // THIS WILL PROVIDE FEEDBACK TO THE USER AFTER
    // WORK BY THIS CLASS HAS COMPLETED
    MessageDialog messageDialog;
    
    // AND WE'LL USE THIS TO ASK YES/NO/CANCEL QUESTIONS
    YesNoCancelDialog yesNoCancelDialog;
    
    // WE'LL USE THIS TO GET OUR VERIFICATION FEEDBACK
    PropertiesManager properties;
    
    public FileController(
            MessageDialog initMessageDialog,
            YesNoCancelDialog initYesNoCancelDialog,
            FileManager initCourseIO) {
        // NOTHING YET
        saved = true;
        
        // KEEP THESE GUYS FOR LATER
        draftIO = initCourseIO;
        
        // AND GET READY TO PROVIDE FEEDBACK
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        properties = PropertiesManager.getPropertiesManager();
        
    }
    
    public void markAsEdited(WBDK_GUI gui) {
        // THE Course OBJECT IS NOW DIRTY
        saved = false;
        
        // LET THE UI KNOW
        gui.updateToolbarControls(saved);
    }
    
    public void handleNewDraftRequest(WBDK_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                DataManager dataManager = gui.getDataManager();
                dataManager.reset();
                saved = false;
                gui.getDisplayTeams().clear();
                gui.setFTeamTableEmpty();
                gui.resetGUI();
                gui.getDraftedPlayers().clear();
                gui.getDraftNameTextField().setText("");
                gui.setTaxiTeamTableEmpty();

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                gui.updateToolbarControls(saved);

                // TELL THE USER THE COURSE HAS BEEN CREATED
                messageDialog.show(properties.getProperty(NEW_DRAFT_CREATED_MESSAGE));
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG, PROVIDE FEEDBACK
            System.out.println("ERROR HANDLING NEW DRAFT");
        }
    }
    
    public void handleSaveDraftRequest(WBDK_GUI gui, Draft draftToSave) {
        try {
            // SAVE IT TO A FILE
            draftIO.saveDraft(gui, draftToSave);

            // MARK IT AS SAVED
            saved = true;

            // TELL THE USER THE FILE HAS BEEN SAVED
            messageDialog.show(properties.getProperty(DRAFT_SAVED_MESSAGE));

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            gui.updateToolbarControls(saved);
        } catch (IOException ioe) {
            System.out.println("ERROR SAVING DRAFT");
        }
    }
    
    public void handleLoadDraftRequest(WBDK_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO OPEN A Course
            if (continueToOpen) {
                // GO AHEAD AND PROCEED LOADING A Course
                promptToOpen(gui);
//                messageDialog.show(properties.getProperty(DRAFT_LOADED_MESSAGE));
//                gui.resetGUI();
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG
            System.out.println("ERROR HANDLING LOAD DRAFT");
        }
    }
    
    public void handleExitRequest(WBDK_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            System.out.println("ERROR HANDLING EXIT");
        }
    }
    
    private boolean promptToSave(WBDK_GUI gui) throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(properties.getProperty(SAVE_UNSAVED_WORK_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            // SAVE THE COURSE
            DataManager dataManager = gui.getDataManager();
            draftIO.saveDraft(gui, dataManager.getDraft());
            //-----------------------------------------------------------------------------------------------------------------------------------------------------
            //NEED TO SAVE PLAYERHOLDER, TEAMS, AND OTHER STUFF TOO
            saved = true;
            
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
    
    private void promptToOpen(WBDK_GUI gui) {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser courseFileChooser = new FileChooser();
        courseFileChooser.setInitialDirectory(new File(PATH_DRAFTS));
        File selectedFile = courseFileChooser.showOpenDialog(gui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
//                gui.resetGUI();
                Draft draftToLoad = gui.getDataManager().getDraft();
                draftIO.loadDraft(draftToLoad, selectedFile.getAbsolutePath());
                gui.reloadDraft(draftToLoad);
                saved = true;
                gui.updateToolbarControls(saved);
                gui.getDraftNameTextField().setText(draftToLoad.getDraftName());
                gui.reloadTeamComboBox();
                messageDialog.show(properties.getProperty(DRAFT_LOADED_MESSAGE));
                gui.resetGUI();
                gui.getDataManager().getPlayerHolder().calcRankings();
                gui.getDataManager().getTeamHolder().calcRankings();
                
                for (int i = 0; i < gui.getDataManager().getTeamHolder().getTeams().size(); i++) {
                    FantasyTeam team = gui.getDataManager().getTeamHolder().getTeams().get(i);
                    team.setPlayersNeeded(team.calcPlayersNeeded());
                    team.setTotalBudget(team.calcTotalBudget());
                    team.setPricePerPlayer(team.calcPricePerPlayer());
                    team.setStandingsR(team.calcR());
                    team.setStandingsHR(team.calcHR());
                    team.setStandingsRBI(team.calcRBI());
                    team.setStandingsSB(team.calcSB());
                    team.setStandingsBA(team.calcBA());
                    team.setStandingsW(team.calcW());
                    team.setStandingsSV(team.calcSV());
                    team.setStandingsK(team.calcK());
                    team.setStandingsERA(team.calcERA());
                    team.setStandingsWHIP(team.calcWHIP());
                    gui.getDataManager().getTeamHolder().clearRankings();
                    gui.getDataManager().getTeamHolder().calcRankings();
                    team.setTotalPoints(team.calcTotalPoints());
                    team.sortTeamPlayers();
                    
//                    gui.getDisplayPlayers().removeAll(team.getListOfTaxiPlayers());
                }
                
                gui.getDataManager().getPlayerHolder().setEstimatedValues(gui.getDataManager().getTeamHolder());

                for (int i = 0; i < gui.getDataManager().getPlayerHolder().getPlayers().size(); i++) {
//                    System.out.println(gui.getDataManager().getPlayerHolder().getPlayers().get(i).getFirstName() + " " + 
//                            gui.getDataManager().getPlayerHolder().getPlayers().get(i).getLastName() + " " + 
//                            gui.getDataManager().getPlayerHolder().getPlayers().get(i).getPickNumber());
                    if (gui.getDataManager().getPlayerHolder().getPlayers().get(i).getPickNumber() != 0) {
                        gui.getDraftedPlayers().add(gui.getDataManager().getPlayerHolder().getPlayers().get(i));
                    }
                }
                
                gui.updateDraftTable();
                
            } catch (IOException ex) {
                Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void markFileAsNotSaved() {
        saved = false;
    }
    
    public boolean isSaved() {
        return saved;
    }
}
