/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import java.util.Collections;
import java.util.concurrent.locks.ReentrantLock;
import javafx.concurrent.Task;
import static wbdk.WBDK_PropertyType.REMOVE_ITEM_MESSAGE;
import wbdk.data.Player;
import wbdk.data.Pitcher;
import wbdk.data.Hitter;
import wbdk.data.PlayerHolder;
import wbdk.gui.PlayerDialog;
import wbdk.gui.WBDK_GUI;
import wbdk.gui.MessageDialog;
import wbdk.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.DataManager;
import javafx.scene.control.ComboBox;
import wbdk.data.FantasyTeam;
import wbdk.data.TeamHolder;

/**
 *
 * @author Kevin
 */
public class PlayerController {
    PlayerDialog pd;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    ReentrantLock draftLock;
    boolean pauseButtonPressed;
    
    public PlayerController(Stage initPrimaryStage, PlayerHolder pH, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog, TeamHolder tH) {
        //pd = new PlayerDialog(initPrimaryStage, pH, initMessageDialog, tH);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        pauseButtonPressed = false;
    }
    
    public void handleAddPlayerRequest(WBDK_GUI gui, Stage initPrimaryStage, MessageDialog initMessageDialog) {
        DataManager dm = gui.getDataManager();
        PlayerHolder pH = dm.getPlayerHolder();
        TeamHolder tH = dm.getTeamHolder();
        pd = new PlayerDialog(initPrimaryStage, pH, initMessageDialog, tH);
        pd.showAddPlayerDialog();
        
        // DID THE USER CONFIRM?
        if (pd.wasCompleteSelected()) {
            Player player = pd.getPlayer();
            if (player.getPlayingPosition().compareToIgnoreCase("P") == 0) {
                Pitcher addPlayer = new Pitcher();
                addPlayer.setPosition(player.getPlayingPosition());
                addPlayer.setFirstName(player.getFirstName());
                addPlayer.setLastName(player.getLastName());
                addPlayer.setTeam(player.getTeam());
                addPlayer.setNotes("");
                addPlayer.setNationOfBirth("UNKNOWN");
                gui.getDisplayPlayers().add(addPlayer);
                gui.getDataManager().getPlayerHolder().addPitcher(addPlayer);
                gui.getDataManager().getPlayerHolder().addPlayer(addPlayer);
                gui.getDisplayMLB().add(addPlayer);
                Collections.sort(gui.getDisplayPlayers());
            }
            else {
                Hitter addPlayer = new Hitter();
                addPlayer.setQualifiedPosition(player.getPlayingPosition() + "_U");
                addPlayer.setFirstName(player.getFirstName());
                addPlayer.setLastName(player.getLastName());
                addPlayer.setTeam(player.getTeam());
                addPlayer.isHitter();
                addPlayer.setNotes("");
                addPlayer.setNationOfBirth("UNKNOWN");
                gui.getDisplayPlayers().add(addPlayer);
                gui.getDataManager().getPlayerHolder().addHitter(addPlayer);
                gui.getDataManager().getPlayerHolder().addPlayer(addPlayer);
                gui.getDisplayMLB().add(addPlayer);
                Collections.sort(gui.getDisplayPlayers());
            }
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditPlayerRequest(WBDK_GUI gui, Player playerToEdit, TeamHolder tH, Stage initPrimaryStage, MessageDialog initMessageDialog) {
        DataManager dm = gui.getDataManager();
        PlayerHolder pH = dm.getPlayerHolder();
        pd = new PlayerDialog(initPrimaryStage, pH, initMessageDialog, tH);
        pd.showEditPlayerDialog(playerToEdit, tH);
        
        // DID THE USER CONFIRM?
        if (pd.wasCompleteSelected()) {
            Player player = pd.getPlayer();
            playerToEdit.setPlayingPosition(player.getPlayingPosition());
            playerToEdit.setContract(player.getContract());
            playerToEdit.setSalary(player.getSalary());
            playerToEdit.setFTeam(player.getFTeam());
            
            FantasyTeam team = getTeamFromName(tH, playerToEdit);
            
            if (team.getListOfActivePlayers().size() < 23) {
                team.addActivePlayer(playerToEdit);
                if (playerToEdit.getHitter()) {
                    team.addActiveHitter((Hitter)playerToEdit);
    //                team.setStandingsR();
                }
                else {
                    team.addActivePitcher((Pitcher)playerToEdit);
                }

                if (playerToEdit.getContract().compareToIgnoreCase("S2") == 0) {
                    gui.getDraftedPlayers().add(playerToEdit);
                    playerToEdit.setPickNumber(gui.getDraftedPlayers().size());
                }

                int x = team.getCurrentQuota().get(playerToEdit.getPlayingPosition());
                team.getCurrentQuota().remove(playerToEdit.getPlayingPosition());
                team.getCurrentQuota().put(playerToEdit.getPlayingPosition(), x + 1);
                team.sortTeamPlayers();

                team.setPlayersNeeded(team.calcPlayersNeeded());
                team.setTotalBudget(team.calcTotalBudget());
                team.setPricePerPlayer(team.calcPricePerPlayer());
                team.setStandingsR(team.calcR());
                team.setStandingsHR(team.calcHR());
                team.setStandingsRBI(team.calcRBI());
                team.setStandingsSB(team.calcSB());
                team.setStandingsBA(team.calcBA());
                team.setStandingsW(team.calcW());
                team.setStandingsSV(team.calcSV());
                team.setStandingsK(team.calcK());
                team.setStandingsERA(team.calcERA());
                team.setStandingsWHIP(team.calcWHIP());
                tH.clearRankings();
                tH.calcRankings();
                team.setTotalPoints(team.calcTotalPoints());
            }
            else {
                gui.getDraftedPlayers().add(playerToEdit);
                playerToEdit.setPickNumber(gui.getDraftedPlayers().size());
                playerToEdit.setFTeam(team.getTeamName());
                team.addTaxiPlayer(playerToEdit);
                playerToEdit.setSalary(1);
            }
            
            gui.getDisplayPlayers().remove(gui.getPlayerTable().getSelectionModel().getSelectedItem());
            
            pH.setEstimatedValues(tH);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleEditPlayerRequest2(WBDK_GUI gui, Player playerToEdit, TeamHolder tH, Stage initPrimaryStage, MessageDialog initMessageDialog) {
        DataManager dm = gui.getDataManager();
        PlayerHolder pH = dm.getPlayerHolder();
        pd = new PlayerDialog(initPrimaryStage, pH, initMessageDialog, tH);
        pd.showEditPlayerDialog2(playerToEdit, tH);
        
        // DID THE USER CONFIRM?
        if (pd.wasCompleteSelected()) {
            Player player = pd.getPlayer();
            Player oldPlayer = new Player();
            
            oldPlayer.setFTeam(playerToEdit.getFTeam());
            oldPlayer.setPlayingPosition(playerToEdit.getPlayingPosition());
            oldPlayer.setSalary(playerToEdit.getSalary());
            oldPlayer.setContract(playerToEdit.getContract());
            
            playerToEdit.setPlayingPosition(player.getPlayingPosition());
            playerToEdit.setContract(player.getContract());
            playerToEdit.setSalary(player.getSalary());
            playerToEdit.setFTeam(player.getFTeam());
            
            FantasyTeam oldTeam = getTeamFromName(tH, oldPlayer);
            FantasyTeam newTeam = getTeamFromName(tH, playerToEdit);
            
            if (playerToEdit.getFTeam().compareToIgnoreCase("FREE AGENT") == 0) {
                
                gui.getDraftedPlayers().remove(playerToEdit);
                
                gui.getDisplayPlayers().add(playerToEdit);
                playerToEdit.setFTeam("");
//                FantasyTeam oldTeam = getTeamFromName(tH, oldPlayer);
                oldTeam.removeActivePlayer(playerToEdit);
                
                if (playerToEdit.getHitter()) {
                    oldTeam.removeActiveHitter((Hitter)playerToEdit);
                }
                else {
                    oldTeam.removeActivePitcher((Pitcher)playerToEdit);
                }
                
                Collections.sort(gui.getDisplayPlayers());
                
                oldTeam.setPlayersNeeded(oldTeam.calcPlayersNeeded());
                oldTeam.setTotalBudget(oldTeam.calcTotalBudget());
                oldTeam.setPricePerPlayer(oldTeam.calcPricePerPlayer());
                oldTeam.setStandingsR(oldTeam.calcR());
                oldTeam.setStandingsHR(oldTeam.calcHR());
                oldTeam.setStandingsRBI(oldTeam.calcRBI());
                oldTeam.setStandingsSB(oldTeam.calcSB());
                oldTeam.setStandingsBA(oldTeam.calcBA());
                
                int y = oldTeam.getCurrentQuota().get(oldPlayer.getPlayingPosition());
                oldTeam.getCurrentQuota().remove(oldPlayer.getPlayingPosition());
                oldTeam.getCurrentQuota().put(oldPlayer.getPlayingPosition(), y-1);
            }
            
            else {
                
                if (oldPlayer.getContract().compareToIgnoreCase("S2") == 0) {
                    gui.getDraftedPlayers().remove(playerToEdit);
                }
                
                if (playerToEdit.getContract().compareToIgnoreCase("S2") == 0) {
                    gui.getDraftedPlayers().add(playerToEdit);
                    playerToEdit.setPickNumber(gui.getDraftedPlayers().size());
                }
                
                newTeam.addActivePlayer(playerToEdit);
                newTeam.sortTeamPlayers();
                newTeam.setPlayersNeeded(newTeam.calcPlayersNeeded());
                newTeam.setTotalBudget(newTeam.calcTotalBudget());
                newTeam.setPricePerPlayer(newTeam.calcPricePerPlayer());
                newTeam.setStandingsR(newTeam.calcR());
                newTeam.setStandingsHR(newTeam.calcHR());
                newTeam.setStandingsRBI(newTeam.calcRBI());
                newTeam.setStandingsSB(newTeam.calcSB());
                newTeam.setStandingsBA(newTeam.calcBA());
                
                oldTeam.removeActivePlayer(playerToEdit);
                oldTeam.sortTeamPlayers();
                oldTeam.setPlayersNeeded(oldTeam.calcPlayersNeeded());
                oldTeam.setTotalBudget(oldTeam.calcTotalBudget());
                oldTeam.setPricePerPlayer(oldTeam.calcPricePerPlayer());
                oldTeam.setStandingsR(oldTeam.calcR());
                oldTeam.setStandingsHR(oldTeam.calcHR());
                oldTeam.setStandingsRBI(oldTeam.calcRBI());
                oldTeam.setStandingsSB(oldTeam.calcSB());
                oldTeam.setStandingsBA(oldTeam.calcBA());
                
                if (playerToEdit.getHitter()) {
                    newTeam.addActiveHitter((Hitter)playerToEdit);
                    oldTeam.removeActiveHitter((Hitter)playerToEdit);
                }
                else {
                    newTeam.addActivePitcher((Pitcher)playerToEdit);
                    oldTeam.removeActivePitcher((Pitcher)playerToEdit);
                }
                
//                FantasyTeam team = getTeamFromName(tH, playerToEdit);
//                FantasyTeam oldTeam = getTeamFromName(tH, oldPlayer);
                
                int x = newTeam.getCurrentQuota().get(playerToEdit.getPlayingPosition());
                newTeam.getCurrentQuota().remove(playerToEdit.getPlayingPosition());
                newTeam.getCurrentQuota().put(playerToEdit.getPlayingPosition(), x+1);
                
                int y = oldTeam.getCurrentQuota().get(oldPlayer.getPlayingPosition());
                oldTeam.getCurrentQuota().remove(oldPlayer.getPlayingPosition());
                oldTeam.getCurrentQuota().put(oldPlayer.getPlayingPosition(), y-1);
            }
            
            pH.setEstimatedValues(tH);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleEditPlayerRequest3(WBDK_GUI gui, Player playerToEdit, TeamHolder tH, Stage initPrimaryStage, MessageDialog initMessageDialog) {
        DataManager dm = gui.getDataManager();
        PlayerHolder pH = dm.getPlayerHolder();
        pd = new PlayerDialog(initPrimaryStage, pH, initMessageDialog, tH);
        pd.showEditPlayerDialog2(playerToEdit, tH);
        
        // DID THE USER CONFIRM?
        if (pd.wasCompleteSelected()) {
            Player player = pd.getPlayer();
            Player oldPlayer = new Player();
            
            oldPlayer.setFTeam(playerToEdit.getFTeam());
            oldPlayer.setPlayingPosition(playerToEdit.getPlayingPosition());
            oldPlayer.setSalary(playerToEdit.getSalary());
            oldPlayer.setContract(playerToEdit.getContract());
            
            playerToEdit.setPlayingPosition(player.getPlayingPosition());
            playerToEdit.setContract(player.getContract());
            playerToEdit.setSalary(player.getSalary());
            playerToEdit.setFTeam(player.getFTeam());
            
            FantasyTeam oldTeam = getTeamFromName(tH, oldPlayer);
            FantasyTeam newTeam = getTeamFromName(tH, playerToEdit);
            
            if (playerToEdit.getFTeam().compareToIgnoreCase("FREE AGENT") == 0) {
                
                gui.getDraftedPlayers().remove(playerToEdit);
                
                gui.getDisplayPlayers().add(playerToEdit);
                playerToEdit.setFTeam("");
//                FantasyTeam oldTeam = getTeamFromName(tH, oldPlayer);
                oldTeam.removeTaxiPlayer(playerToEdit);
                
                Collections.sort(gui.getDisplayPlayers());
                
                oldTeam.setPlayersNeeded(oldTeam.calcPlayersNeeded());
                oldTeam.setTotalBudget(oldTeam.calcTotalBudget());
                oldTeam.setPricePerPlayer(oldTeam.calcPricePerPlayer());
                oldTeam.setStandingsR(oldTeam.calcR());
                oldTeam.setStandingsHR(oldTeam.calcHR());
                oldTeam.setStandingsRBI(oldTeam.calcRBI());
                oldTeam.setStandingsSB(oldTeam.calcSB());
                oldTeam.setStandingsBA(oldTeam.calcBA());
            }
            
            else {
                
//                if (oldPlayer.getContract().compareToIgnoreCase("S2") == 0) {
//                    gui.getDraftedPlayers().remove(playerToEdit);
//                }
                
                if (oldPlayer.getPickNumber() != 1) {
                    gui.getDraftedPlayers().remove(playerToEdit);
                }
                
                if (playerToEdit.getContract().compareToIgnoreCase("S2") == 0) {
                    gui.getDraftedPlayers().add(playerToEdit);
                    playerToEdit.setPickNumber(gui.getDraftedPlayers().size());
                }
                
                newTeam.addActivePlayer(playerToEdit);
                newTeam.sortTeamPlayers();
                newTeam.setPlayersNeeded(newTeam.calcPlayersNeeded());
                newTeam.setTotalBudget(newTeam.calcTotalBudget());
                newTeam.setPricePerPlayer(newTeam.calcPricePerPlayer());
                newTeam.setStandingsR(newTeam.calcR());
                newTeam.setStandingsHR(newTeam.calcHR());
                newTeam.setStandingsRBI(newTeam.calcRBI());
                newTeam.setStandingsSB(newTeam.calcSB());
                newTeam.setStandingsBA(newTeam.calcBA());
                
                oldTeam.removeTaxiPlayer(playerToEdit);
                
                if (playerToEdit.getHitter()) {
                    newTeam.addActiveHitter((Hitter)playerToEdit);
                }
                else {
                    newTeam.addActivePitcher((Pitcher)playerToEdit);
                }
                
                int x = newTeam.getCurrentQuota().get(playerToEdit.getPlayingPosition());
                newTeam.getCurrentQuota().remove(playerToEdit.getPlayingPosition());
                newTeam.getCurrentQuota().put(playerToEdit.getPlayingPosition(), x+1);
            }
            
            pH.setEstimatedValues(tH);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public FantasyTeam getTeamFromName(TeamHolder tH, Player playerToEdit) {
        FantasyTeam team = new FantasyTeam();
        for (int i = 0; i < tH.getTeams().size(); i++) {
            if (tH.getTeams().get(i).getTeamName().compareToIgnoreCase(playerToEdit.getFTeam()) == 0) {
                team = tH.getTeams().get(i);
                    
            }
        }
        return team;
    }
    
    public void draftPlayer(WBDK_GUI gui, TeamHolder tH, PlayerHolder pH) {
        
        for (int i = 0; i < tH.getTeams().size(); i++) {
            
            if (tH.getTeams().get(i).getCurrentQuota().get("C") != tH.getTeams().get(i).getMaxQuota().get("C")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("C") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("C");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("C");
                            tH.getTeams().get(i).getCurrentQuota().remove("C");
                            tH.getTeams().get(i).getCurrentQuota().put("C", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("1B") != tH.getTeams().get(i).getMaxQuota().get("1B")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("1B") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("1B");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("1B");
                            tH.getTeams().get(i).getCurrentQuota().remove("1B");
                            tH.getTeams().get(i).getCurrentQuota().put("1B", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("CI") != tH.getTeams().get(i).getMaxQuota().get("CI")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("CI") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("CI");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("CI");
                            tH.getTeams().get(i).getCurrentQuota().remove("CI");
                            tH.getTeams().get(i).getCurrentQuota().put("CI", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("3B") != tH.getTeams().get(i).getMaxQuota().get("3B")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("3B") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("3B");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("3B");
                            tH.getTeams().get(i).getCurrentQuota().remove("3B");
                            tH.getTeams().get(i).getCurrentQuota().put("3B", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("2B") != tH.getTeams().get(i).getMaxQuota().get("2B")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("2B") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("2B");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("2B");
                            tH.getTeams().get(i).getCurrentQuota().remove("2B");
                            tH.getTeams().get(i).getCurrentQuota().put("2B", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("MI") != tH.getTeams().get(i).getMaxQuota().get("MI")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("MI") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("MI");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("MI");
                            tH.getTeams().get(i).getCurrentQuota().remove("MI");
                            tH.getTeams().get(i).getCurrentQuota().put("MI", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("SS") != tH.getTeams().get(i).getMaxQuota().get("SS")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("SS") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("SS");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("SS");
                            tH.getTeams().get(i).getCurrentQuota().remove("SS");
                            tH.getTeams().get(i).getCurrentQuota().put("SS", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("OF") != tH.getTeams().get(i).getMaxQuota().get("OF")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("OF") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("OF");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("OF");
                            tH.getTeams().get(i).getCurrentQuota().remove("OF");
                            tH.getTeams().get(i).getCurrentQuota().put("OF", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("U") != tH.getTeams().get(i).getMaxQuota().get("U")) {
                
                for (int j = 0; j < pH.getHitters().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    String[] positions = pH.getHitters().get(j).getQPList();
                    
                    for (int h = 0; h < positions.length; h++) {
                        if (positions[h].compareToIgnoreCase("U") == 0 && pH.getHitters().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActiveHitter(pH.getHitters().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPlayingPosition("U");
                            pH.getHitters().get(j).setSalary(1);
                            pH.getHitters().get(j).setContract("S2");
                            pH.getHitters().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("U");
                            tH.getTeams().get(i).getCurrentQuota().remove("U");
                            tH.getTeams().get(i).getCurrentQuota().put("U", x + 1);
                            gui.getDraftedPlayers().add(pH.getHitters().get(j));
                            gui.getDisplayPlayers().remove(pH.getHitters().get(j));
                            pH.getHitters().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                    }
                }
            }
            
            if (tH.getTeams().get(i).getCurrentQuota().get("P") != tH.getTeams().get(i).getMaxQuota().get("P")) {
                
                for (int j = 0; j < pH.getPitchers().size(); j++) { //REPLACE pH.getHitters() with HitterRankings() if you want best ranking players
                    if (pH.getPitchers().get(j).getPosition().compareToIgnoreCase("P") == 0 && pH.getPitchers().get(j).getFTeam().equalsIgnoreCase("")) {
                            tH.getTeams().get(i).addActivePitcher(pH.getPitchers().get(j));
                            tH.getTeams().get(i).addActivePlayer(pH.getPitchers().get(j));
                            pH.getPitchers().get(j).setPlayingPosition("P");
                            pH.getPitchers().get(j).setSalary(1);
                            pH.getPitchers().get(j).setContract("S2");
                            pH.getPitchers().get(j).setFTeam(tH.getTeams().get(i).getTeamName());
                            
                            int x = tH.getTeams().get(i).getCurrentQuota().get("P");
                            tH.getTeams().get(i).getCurrentQuota().remove("P");
                            tH.getTeams().get(i).getCurrentQuota().put("P", x + 1);
                            gui.getDraftedPlayers().add(pH.getPitchers().get(j));
                            gui.getDisplayPlayers().remove(pH.getPitchers().get(j));
                            pH.getPitchers().get(j).setPickNumber(gui.getDraftedPlayers().size());
//                            System.out.println("ADDED PLAYER " + pH.getHitters().get(j).getFirstName() + " " +
//                                    pH.getHitters().get(j).getLastName() + " TO TEAM " + pH.getHitters().get(j).getFTeam());
                            tH.getTeams().get(i).setPlayersNeeded(tH.getTeams().get(i).calcPlayersNeeded());
                            tH.getTeams().get(i).setTotalBudget(tH.getTeams().get(i).calcTotalBudget());
                            tH.getTeams().get(i).setPricePerPlayer(tH.getTeams().get(i).calcPricePerPlayer());
                            tH.getTeams().get(i).setStandingsR(tH.getTeams().get(i).calcR());
                            tH.getTeams().get(i).setStandingsHR(tH.getTeams().get(i).calcHR());
                            tH.getTeams().get(i).setStandingsRBI(tH.getTeams().get(i).calcRBI());
                            tH.getTeams().get(i).setStandingsSB(tH.getTeams().get(i).calcSB());
                            tH.getTeams().get(i).setStandingsBA(tH.getTeams().get(i).calcBA());
                            tH.getTeams().get(i).setStandingsW(tH.getTeams().get(i).calcW());
                            tH.getTeams().get(i).setStandingsSV(tH.getTeams().get(i).calcSV());
                            tH.getTeams().get(i).setStandingsK(tH.getTeams().get(i).calcK());
                            tH.getTeams().get(i).setStandingsERA(tH.getTeams().get(i).calcERA());
                            tH.getTeams().get(i).setStandingsWHIP(tH.getTeams().get(i).calcWHIP());
                            tH.clearRankings();
                            tH.calcRankings();
//                            tH.getTeams().get(i).setTotalPoints(tH.getTeams().get(i).calcTotalPoints());
                            for (int a = 0; a < tH.getTeams().size(); a++) {
                                tH.getTeams().get(a).setTotalPoints(tH.getTeams().get(a).calcTotalPoints());
                            }
                            return;
                        }
                }
            }
        }
        
        draftTaxi(gui, tH, pH);
    }
    
    public void draftTaxi(WBDK_GUI gui, TeamHolder tH, PlayerHolder pH) {
        for (int i = 0; i < tH.getTeams().size(); i++) {
            if (tH.getTeams().get(i).calcPlayersNeeded() == 0 && tH.getTeams().get(i).getListOfTaxiPlayers().size() != 8) {
                
                for (int j = 0; j < pH.getPlayers().size(); j++) {
                    if (pH.getPlayers().get(j).getFTeam().equalsIgnoreCase("")) {
                        if (pH.getPlayers().get(j).getHitter()) {
                            Hitter hitter = (Hitter)pH.getPlayers().get(j);
                            String[] positions = hitter.getQPList();
                            
                            int randomNumb = (int)(Math.random() * positions.length);
                            hitter.setPlayingPosition(positions[randomNumb]);
                            hitter.setSalary(1);
                            hitter.setContract("X");
                            hitter.setFTeam(tH.getTeams().get(i).getTeamName());
                            gui.getDraftedPlayers().add(hitter);
                            tH.getTeams().get(i).addTaxiPlayer(hitter);
                            hitter.setPickNumber(gui.getDraftedPlayers().size());
                            gui.getDisplayPlayers().remove(pH.getPlayers().get(j));
                        }
                        else {
                            Pitcher pitcher = (Pitcher)pH.getPlayers().get(j);
                            
                            pitcher.setPlayingPosition("P");
                            pitcher.setSalary(1);
                            pitcher.setContract("X");
                            pitcher.setFTeam(tH.getTeams().get(i).getTeamName());
                            gui.getDraftedPlayers().add(pitcher);
                            tH.getTeams().get(i).addTaxiPlayer(pitcher);
                            pitcher.setPickNumber(gui.getDraftedPlayers().size());
                            gui.getDisplayPlayers().remove(pH.getPlayers().get(j));
                        }
                        return;
                    }
                }
            }
        }
    }

    public void automatedDraft (WBDK_GUI gui, TeamHolder tH, PlayerHolder pH) {
        int playersNeeded = tH.getHittersRequired() + tH.getPitchersRequired();
        int taxiNeeded = tH.getTaxiRequired();
        draftLock = new ReentrantLock();
        
        
        Task<Void> task = new Task<Void>() {
            
            @Override
            protected Void call() throws Exception {
                try {
                        draftLock.lock();
                        for (int i = 0; i < playersNeeded; i++) {
                            if (pauseButtonPressed) {
                                pauseButtonPressed = false;
                                break;
                            }
                            draftPlayer(gui, tH, pH);
                            Thread.sleep(100);
                        }
                        
                        for (int i = 0; i < taxiNeeded; i++) {
                            if (pauseButtonPressed) {
                                pauseButtonPressed = false;
                                break;
                            }
                            draftPlayer(gui, tH, pH);
                            Thread.sleep(100);
                        }
                    }
                    finally {
                            draftLock.unlock();
                            }
                return null;
            }
        };
                 //THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();
//                task.setOnSucceeded(e->{
//                    
//                });
                
    }
    
    public void isPauseButtonPressed() {
        pauseButtonPressed = true;
    }
}
