/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk;

import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.DataManager;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.file.JsonFileManager;
import wbdk.gui.WBDK_GUI;
import static wbdk.WBDK_PropertyType.PROP_APP_TITLE;
import static wbdk.WBDK_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wbdk.WBDK_StartupConstants.JSON_FILE_PATH_PITCHERS;
import static wbdk.WBDK_StartupConstants.PATH_DATA;
import static wbdk.WBDK_StartupConstants.PROPERTIES_FILE_NAME;
import static wbdk.WBDK_StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import wbdk.data.Draft;
import wbdk.data.PlayerHolder;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author Kevin
 */
public class WolfieBallDraftKit extends Application {
    
    WBDK_GUI gui;
    
    @Override
    public void start(Stage primaryStage) {
         // LET'S START BY GIVING THE PRIMARY STAGE TO OUR ERROR HANDLER
        
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
            try {                
                // WE WILL SAVE OUR COURSE DATA USING THE JSON FILE
                // FORMAT SO WE'LL LET THIS OBJECT DO THIS FOR US
//                JsonFileManager jsonFileManager = new JsonFileManager();
//               
//                Draft draft = new Draft();
//                draft.setPlayerHolder(jsonFileManager.loadPlayerHolder(JSON_FILE_PATH_PITCHERS, JSON_FILE_PATH_HITTERS));
//                                
//                // AND NOW GIVE ALL OF THIS STUFF TO THE GUI
//                // INITIALIZE THE USER INTERFACE COMPONENTS
//                gui = new WBDK_GUI(primaryStage);
//                gui.setFileManager(jsonFileManager);
//                
//                // CONSTRUCT THE DATA MANAGER AND GIVE IT TO THE GUI
//                DataManager dataManager = new DataManager(gui);
//                dataManager.setDraft(draft);
//                gui.setDataManager(dataManager);
                
                
                gui = new WBDK_GUI(primaryStage);
                DataManager dataManager = new DataManager(gui);
                JsonFileManager jsonFileManager = dataManager.getJSONFileManager();
               
                Draft draft = new Draft();
                draft.setPlayerHolder(jsonFileManager.loadPlayerHolder(JSON_FILE_PATH_PITCHERS, JSON_FILE_PATH_HITTERS));
                
                dataManager.setDraft(draft);
                gui.setDataManager(dataManager);
                
                
                gui.setFileManager(jsonFileManager);
                
                
                

                // FINALLY, START UP THE USER INTERFACE WINDOW AFTER ALL
                // REMAINING INITIALIZATION
                gui.initGUI(appTitle);                
            }
            catch(IOException ioe) {
                ioe.printStackTrace();
            }
        }
   }
    
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            return false;
        }        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
