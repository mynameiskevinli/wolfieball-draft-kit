/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.file;

import static wbdk.WBDK_StartupConstants.PATH_DRAFTS;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import wbdk.data.Draft;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.data.Player;
import java.util.List;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;
import static wbdk.WBDK_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wbdk.WBDK_StartupConstants.JSON_FILE_PATH_PITCHERS;
import wbdk.data.FantasyTeam;
import wbdk.data.PlayerHolder;
import wbdk.data.TeamHolder;
import wbdk.gui.WBDK_GUI;

/**
 *
 * @author Kevin
 */
public class JsonFileManager implements FileManager{
    
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";
    String JSON_TEAM = "TEAM";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_IP = "IP";
    String JSON_ER = "ER";
    String JSON_W = "W";
    String JSON_SV = "SV";
    String JSON_H = "H";
    String JSON_BB = "BB";
    String JSON_K = "K";
    String JSON_NOTES = "NOTES";
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    String JSON_QP = "QP";
    String JSON_AB = "AB";
    String JSON_R = "R";
    String JSON_HR = "HR";
    String JSON_RBI = "RBI";
    String JSON_SB = "SB";
    String JSON_FANTASY_TEAM = "FANTASY_TEAM";
    String JSON_TEAM_OWNER = "TEAM_OWNER";
    String JSON_TEAM_NAME = "TEAM_NAME";
    String JSON_LIST_OF_FANTASY_TEAMS = "LIST_OF_FANTASY_TEAMS";
    String JSON_DRAFT_NAME = "DRAFT_NAME";
    String JSON_POSITION = "POSITION";
    String JSON_CONTRACT = "CONTRACT";
    String JSON_SALARY = "SALARY";
    String JSON_QUALIFIED_POSITION = "QUALIFIED POSITION";
    String JSON_CURRENT_QUOTA = "CURRENT_QUOTA";
    String JSON_CURRENT_C = "CURRENT_C";
    String JSON_CURRENT_1B = "CURRENT_1B";
    String JSON_CURRENT_CI = "CURRENT_CI";
    String JSON_CURRENT_3B = "CURRENT_3B";
    String JSON_CURRENT_2B = "CURRENT_2B";
    String JSON_CURRENT_MI = "CURRENT_MI";
    String JSON_CURRENT_SS = "CURRENT_SS";
    String JSON_CURRENT_U = "CURRENT_U";
    String JSON_CURRENT_OF = "CURRENT_OF";
    String JSON_CURRENT_P = "CURRENT_P";
    String JSON_FREE_PITCHERS = "FREE AGENT PITCHERS";
    String JSON_FREE_HITTERS = "FREE AGENT HITTERS";
    String JSON_FTEAM = "FTEAM";
    String JSON_TAXI_SQUAD = "TAXI_SQUAD";
    String JSON_EST_VALUE = "ESTIMATED_VALUE";
    String JSON_PICK_NUMBER = "PICK_NUMBER";
     
    String JSON_EXT = ".json";
    String SLASH = "/";

    @Override
    public void saveDraft(WBDK_GUI gui, Draft draftToSave) throws IOException {
        String draftName = "" + draftToSave.getDraftName();
        String jsonFilePath = PATH_DRAFTS + SLASH + draftName + JSON_EXT;
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        gui.fillUpDisplayHittersAndPitchers();
        
        JsonArray fantasyTeamsArray = makeFantasyTeamsJsonArray(draftToSave.getTeamHolder().getTeams());
        JsonArray freePitchersArray = makePitchersJsonArray(gui.getDisplayPitchers());
        JsonArray freeHittersArray = makeHittersJsonArray(gui.getDisplayHitters());
        
        JsonObject draftJsonObject = Json.createObjectBuilder()
                                    .add(JSON_DRAFT_NAME, draftToSave.getDraftName())
                                    .add(JSON_LIST_OF_FANTASY_TEAMS, fantasyTeamsArray)
                                    .add(JSON_FREE_HITTERS, freeHittersArray)
                                    .add(JSON_FREE_PITCHERS, freePitchersArray)
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(draftJsonObject);
    }

    @Override
    public void loadDraft(Draft draftToLoad, String coursePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(coursePath);
        
        draftToLoad.setDraftName(json.getString(JSON_DRAFT_NAME));
//        draftToLoad.setPlayerHolder(loadPlayerHolder(JSON_FILE_PATH_PITCHERS, JSON_FILE_PATH_HITTERS));
        TeamHolder tH = new TeamHolder();
        draftToLoad.setTeamHolder(tH);
        PlayerHolder pH = new PlayerHolder();
        draftToLoad.setPlayerHolder(pH);
        
        JsonArray jsonFantasyTeamsArray = json.getJsonArray(JSON_LIST_OF_FANTASY_TEAMS);
        for (int i = 0; i < jsonFantasyTeamsArray.size(); i++) {
            JsonObject jso = jsonFantasyTeamsArray.getJsonObject(i);
            FantasyTeam fT = new FantasyTeam();
            fT.setTeamName(jso.getString(JSON_TEAM_NAME));
            fT.setTeamOwner(jso.getString(JSON_TEAM_OWNER));
            
            JsonArray jsonPitchersArray = jso.getJsonArray(JSON_PITCHERS);
                if (jsonPitchersArray != null) {
                    for (int j = 0; j < jsonPitchersArray.size(); j++) {
                        JsonObject jso2 = jsonPitchersArray.getJsonObject(j);
                        Pitcher p = new Pitcher();
                        p.setTeam(jso2.getString(JSON_TEAM));
                        p.setLastName(jso2.getString(JSON_LAST_NAME));
                        p.setFirstName(jso2.getString(JSON_FIRST_NAME));
                        p.setInningsPitched(jso2.getJsonNumber(JSON_IP).doubleValue());
                        p.setEarnedRuns(jso2.getInt(JSON_ER));
                        p.setWins(jso2.getInt(JSON_W));
                        p.setSaves(jso2.getInt(JSON_SV));
                        p.setHits(jso2.getInt(JSON_H));
                        p.setBasesOnBall(jso2.getInt(JSON_BB));
                        p.setStrikeouts(jso2.getInt(JSON_K));
                        p.setNotes(jso2.getString(JSON_NOTES));
                        p.setYearOfBirth(jso2.getInt(JSON_YEAR_OF_BIRTH));
                        p.calculateERA(p.getEarnedRuns(), p.getInningsPitched());
                        p.calculateWHIP(p.getWalks(), p.getHits(), p.getInningsPitched());
                        p.setNationOfBirth(jso2.getString(JSON_NATION_OF_BIRTH));
                        p.setPlayingPosition(jso2.getString(JSON_POSITION));
                        p.setPosition(jso2.getString(JSON_POSITION));
                        p.setContract(jso2.getString(JSON_CONTRACT));
                        p.setSalary(jso2.getInt(JSON_SALARY));
                        p.setFTeam(jso2.getString(JSON_FTEAM));
                        p.setEstimatedValue(jso2.getInt(JSON_EST_VALUE));
                        p.setPickNumber(jso2.getInt(JSON_PICK_NUMBER));

                        fT.addActivePlayer(p);
                        fT.addActivePitcher(p);
                        pH.addPlayer(p);
                        pH.addPitcher(p);
                        
                }
            }
            
            JsonArray jsonHittersArray = jso.getJsonArray(JSON_HITTERS);
                if (jsonHittersArray != null) {
                    for (int k = 0; k < jsonHittersArray.size(); k++) {
                        JsonObject jso3 = jsonHittersArray.getJsonObject(k);
                        Hitter h = new Hitter();
                        h.setTeam(jso3.getString(JSON_TEAM));
                        h.setLastName(jso3.getString(JSON_LAST_NAME));
                        h.setFirstName(jso3.getString(JSON_FIRST_NAME));
                        h.setQualifiedPosition(jso3.getString(JSON_QUALIFIED_POSITION));
                        h.setAtBats(jso3.getInt(JSON_AB));
                        h.setRuns(jso3.getInt(JSON_R));
                        h.setHits(jso3.getInt(JSON_H));
                        h.setHomeruns(jso3.getInt(JSON_HR));
                        h.setRunsBattedIn(jso3.getInt(JSON_RBI));
                        h.setStolenBases(jso3.getInt(JSON_SB));
                        h.setNotes(jso3.getString(JSON_NOTES));
                        h.setYearOfBirth(jso3.getInt(JSON_YEAR_OF_BIRTH));
                        h.setNationOfBirth(jso3.getString(JSON_NATION_OF_BIRTH));
                        h.calculateBA(h.getHits(), h.getAtBats());
                        h.setPlayingPosition(jso3.getString(JSON_POSITION));
                        h.setContract(jso3.getString(JSON_CONTRACT));
                        h.setSalary(jso3.getInt(JSON_SALARY));
                        h.isHitter();
                        h.setFTeam(jso3.getString(JSON_FTEAM));
                        h.setEstimatedValue(jso3.getInt(JSON_EST_VALUE));
                        h.setPickNumber(jso3.getInt(JSON_PICK_NUMBER));

                        fT.addActivePlayer(h);
                        fT.addActiveHitter(h);
                        pH.addPlayer(h);
                        pH.addHitter(h);
                        
                }
            }
            
            JsonObject jsonCurrentQuota = jso.getJsonObject(JSON_CURRENT_QUOTA);
            Map currentQuota = new HashMap<String,Integer>();
            currentQuota.put("C", jsonCurrentQuota.getInt(JSON_CURRENT_C));
            currentQuota.put("1B", jsonCurrentQuota.getInt(JSON_CURRENT_1B));
            currentQuota.put("CI", jsonCurrentQuota.getInt(JSON_CURRENT_CI));
            currentQuota.put("3B", jsonCurrentQuota.getInt(JSON_CURRENT_3B));
            currentQuota.put("2B", jsonCurrentQuota.getInt(JSON_CURRENT_2B));
            currentQuota.put("MI", jsonCurrentQuota.getInt(JSON_CURRENT_MI));
            currentQuota.put("SS", jsonCurrentQuota.getInt(JSON_CURRENT_SS));
            currentQuota.put("U", jsonCurrentQuota.getInt(JSON_CURRENT_U));
            currentQuota.put("OF", jsonCurrentQuota.getInt(JSON_CURRENT_OF));
            currentQuota.put("P", jsonCurrentQuota.getInt(JSON_CURRENT_P));
            fT.setCurrentQuota(currentQuota);
            
            JsonArray jsonTaxiArray = jso.getJsonArray(JSON_TAXI_SQUAD);
                if (jsonTaxiArray != null) {
                    for (int z = 0; z < jsonTaxiArray.size(); z++) {
                        JsonObject jso4 = jsonTaxiArray.getJsonObject(z);
                        if ((jso4.getString(JSON_POSITION)).equalsIgnoreCase("P")) {
                            Pitcher p = new Pitcher();
                            p.setTeam(jso4.getString(JSON_TEAM));
                            p.setLastName(jso4.getString(JSON_LAST_NAME));
                            p.setFirstName(jso4.getString(JSON_FIRST_NAME));
                            p.setInningsPitched(jso4.getJsonNumber(JSON_IP).doubleValue());
                            p.setEarnedRuns(jso4.getInt(JSON_ER));
                            p.setWins(jso4.getInt(JSON_W));
                            p.setSaves(jso4.getInt(JSON_SV));
                            p.setHits(jso4.getInt(JSON_H));
                            p.setBasesOnBall(jso4.getInt(JSON_BB));
                            p.setStrikeouts(jso4.getInt(JSON_K));
                            p.setNotes(jso4.getString(JSON_NOTES));
                            p.setYearOfBirth(jso4.getInt(JSON_YEAR_OF_BIRTH));
                            p.calculateERA(p.getEarnedRuns(), p.getInningsPitched());
                            p.calculateWHIP(p.getWalks(), p.getHits(), p.getInningsPitched());
                            p.setNationOfBirth(jso4.getString(JSON_NATION_OF_BIRTH));
                            p.setPlayingPosition(jso4.getString(JSON_POSITION));
                            p.setPosition(jso4.getString(JSON_POSITION));
                            p.setContract(jso4.getString(JSON_CONTRACT));
                            p.setSalary(jso4.getInt(JSON_SALARY));
                            p.setFTeam(jso4.getString(JSON_FTEAM));
                            p.setEstimatedValue(jso4.getInt(JSON_EST_VALUE));
                            p.setPickNumber(jso4.getInt(JSON_PICK_NUMBER));

                            fT.addTaxiPlayer(p);
                            pH.addPlayer(p);
                            pH.addPitcher(p);
                        }
                        else {
                            Hitter h = new Hitter();
                            h.setTeam(jso4.getString(JSON_TEAM));
                            h.setLastName(jso4.getString(JSON_LAST_NAME));
                            h.setFirstName(jso4.getString(JSON_FIRST_NAME));
                            h.setQualifiedPosition(jso4.getString(JSON_QUALIFIED_POSITION));
                            h.setAtBats(jso4.getInt(JSON_AB));
                            h.setRuns(jso4.getInt(JSON_R));
                            h.setHits(jso4.getInt(JSON_H));
                            h.setHomeruns(jso4.getInt(JSON_HR));
                            h.setRunsBattedIn(jso4.getInt(JSON_RBI));
                            h.setStolenBases(jso4.getInt(JSON_SB));
                            h.setNotes(jso4.getString(JSON_NOTES));
                            h.setYearOfBirth(jso4.getInt(JSON_YEAR_OF_BIRTH));
                            h.setNationOfBirth(jso4.getString(JSON_NATION_OF_BIRTH));
                            h.calculateBA(h.getHits(), h.getAtBats());
                            h.setPlayingPosition(jso4.getString(JSON_POSITION));
                            h.setContract(jso4.getString(JSON_CONTRACT));
                            h.setSalary(jso4.getInt(JSON_SALARY));
                            h.isHitter();
                            h.setFTeam(jso4.getString(JSON_FTEAM));
                            h.setEstimatedValue(jso4.getInt(JSON_EST_VALUE));
                            h.setPickNumber(jso4.getInt(JSON_PICK_NUMBER));

                            fT.addTaxiPlayer(h);
                            pH.addPlayer(h);
                            pH.addHitter(h);
                        }
                    }
                }
            
            // ADD IT TO THE TEAMHOLDER
            tH.addTeam(fT);
        }
        
        JsonArray jsonFreeHittersArray = json.getJsonArray(JSON_FREE_HITTERS);
        for (int i = 0; i < jsonFreeHittersArray.size(); i++) {
            JsonObject jso = jsonFreeHittersArray.getJsonObject(i);
            Hitter hitter = new Hitter();
            hitter.setTeam(jso.getString(JSON_TEAM));
            hitter.setLastName(jso.getString(JSON_LAST_NAME));
            hitter.setFirstName(jso.getString(JSON_FIRST_NAME));
            hitter.setQualifiedPosition(jso.getString(JSON_QUALIFIED_POSITION));
            hitter.setAtBats(jso.getInt(JSON_AB));
            hitter.setRuns(jso.getInt(JSON_R));
            hitter.setHits(jso.getInt(JSON_H));
            hitter.setHomeruns(jso.getInt(JSON_HR));
            hitter.setRunsBattedIn(jso.getInt(JSON_RBI));
            hitter.setStolenBases(jso.getInt(JSON_SB));
            hitter.setNotes(jso.getString(JSON_NOTES));
            hitter.setYearOfBirth(jso.getInt(JSON_YEAR_OF_BIRTH));
            hitter.setNationOfBirth(jso.getString(JSON_NATION_OF_BIRTH));
            hitter.calculateBA(hitter.getHits(), hitter.getAtBats());
            hitter.setContract("");
            hitter.isHitter();
            
            pH.addHitter(hitter);
            pH.addPlayer(hitter);
        }
        
        JsonArray jsonFreePitchersArray = json.getJsonArray(JSON_FREE_PITCHERS);
        for (int i = 0; i < jsonFreePitchersArray.size(); i++) {
            JsonObject jso = jsonFreePitchersArray.getJsonObject(i);
            Pitcher pitcher = new Pitcher();
            pitcher.setTeam(jso.getString(JSON_TEAM));
            pitcher.setLastName(jso.getString(JSON_LAST_NAME));
            pitcher.setFirstName(jso.getString(JSON_FIRST_NAME));
            pitcher.setInningsPitched(jso.getJsonNumber(JSON_IP).doubleValue());
            pitcher.setEarnedRuns(jso.getInt(JSON_ER));
            pitcher.setWins(jso.getInt(JSON_W));
            pitcher.setSaves(jso.getInt(JSON_SV));
            pitcher.setHits(jso.getInt(JSON_H));
            pitcher.setBasesOnBall(jso.getInt(JSON_BB));
            pitcher.setStrikeouts(jso.getInt(JSON_K));
            pitcher.setNotes(jso.getString(JSON_NOTES));
            pitcher.setYearOfBirth(jso.getInt(JSON_YEAR_OF_BIRTH));
            pitcher.setNationOfBirth(jso.getString(JSON_NATION_OF_BIRTH));
            pitcher.calculateERA(pitcher.getEarnedRuns(), pitcher.getInningsPitched());
            pitcher.calculateWHIP(pitcher.getWalks(), pitcher.getHits(), pitcher.getInningsPitched());
            //pitcher.setPosition(jso.getString(JSON_POSITION));
            pitcher.setPosition("P");
            pitcher.setContract("");
            
            pH.addPitcher(pitcher);
            pH.addPlayer(pitcher);
        }
        
    }

    @Override
    public void loadHitters(String filePath, PlayerHolder phToLoad) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);
        
        // NOW LOAD THE PITCHERS
        //phToLoad.clearHitters();
        JsonArray jsonHittersArray = json.getJsonArray(JSON_HITTERS);
        for (int i = 0; i < jsonHittersArray.size(); i++) {
            JsonObject jso = jsonHittersArray.getJsonObject(i);
            Hitter hitter = new Hitter();
            hitter.setTeam(jso.getString(JSON_TEAM));
            hitter.setLastName(jso.getString(JSON_LAST_NAME));
            hitter.setFirstName(jso.getString(JSON_FIRST_NAME));
            if ((jso.getString(JSON_QP).contains("2B") || jso.getString(JSON_QP).contains("SS")) &&
                    (jso.getString(JSON_QP).contains("1B") || jso.getString(JSON_QP).contains("3B"))) {
                hitter.setQualifiedPosition(jso.getString(JSON_QP) + "_CI_MI_U");
            }
            else if (jso.getString(JSON_QP).contains("2B") || jso.getString(JSON_QP).contains("SS")) {
                hitter.setQualifiedPosition(jso.getString(JSON_QP) + "_MI_U");
            }
            else if (jso.getString(JSON_QP).contains("1B") || jso.getString(JSON_QP).contains("3B")) {
                hitter.setQualifiedPosition(jso.getString(JSON_QP) + "_CI_U");
            }
            else {
                hitter.setQualifiedPosition(jso.getString(JSON_QP) + "_U");
            }
            hitter.setAtBats(Integer.parseInt(jso.getString(JSON_AB)));
            hitter.setRuns(Integer.parseInt(jso.getString(JSON_R)));
            hitter.setHits(Integer.parseInt(jso.getString(JSON_H)));
            hitter.setHomeruns(Integer.parseInt(jso.getString(JSON_HR)));
            hitter.setRunsBattedIn(Integer.parseInt(jso.getString(JSON_RBI)));
            hitter.setStolenBases(Integer.parseInt(jso.getString(JSON_SB)));
            hitter.setNotes(jso.getString(JSON_NOTES));
            hitter.setYearOfBirth(Integer.parseInt(jso.getString(JSON_YEAR_OF_BIRTH)));
            hitter.setNationOfBirth(jso.getString(JSON_NATION_OF_BIRTH));
            hitter.calculateBA(hitter.getHits(), hitter.getAtBats());
            hitter.setContract("");
            hitter.isHitter();
            
            
            // ADD IT TO THE COURSE
            phToLoad.addHitter(hitter);
            phToLoad.addPlayer(hitter);
        }
    }

    @Override
    public void loadPitchers(String filePath, PlayerHolder phToLoad) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);
        
        // NOW LOAD THE PITCHERS
        //phToLoad.clearPitchers();
        JsonArray jsonPitchersArray = json.getJsonArray(JSON_PITCHERS);
        for (int i = 0; i < jsonPitchersArray.size(); i++) {
            JsonObject jso = jsonPitchersArray.getJsonObject(i);
            Pitcher pitcher = new Pitcher();
            pitcher.setTeam(jso.getString(JSON_TEAM));
            pitcher.setLastName(jso.getString(JSON_LAST_NAME));
            pitcher.setFirstName(jso.getString(JSON_FIRST_NAME));
            pitcher.setInningsPitched(Float.parseFloat(jso.getString(JSON_IP)));
            pitcher.setEarnedRuns(Integer.parseInt(jso.getString(JSON_ER)));
            pitcher.setWins(Integer.parseInt(jso.getString(JSON_W)));
            pitcher.setSaves(Integer.parseInt(jso.getString(JSON_SV)));
            pitcher.setHits(Integer.parseInt(jso.getString(JSON_H)));
            pitcher.setBasesOnBall(Integer.parseInt(jso.getString(JSON_BB)));
            pitcher.setStrikeouts(Integer.parseInt(jso.getString(JSON_K)));
            pitcher.setNotes(jso.getString(JSON_NOTES));
            pitcher.setYearOfBirth(Integer.parseInt(jso.getString(JSON_YEAR_OF_BIRTH)));
            pitcher.setNationOfBirth(jso.getString(JSON_NATION_OF_BIRTH));
            pitcher.calculateERA(pitcher.getEarnedRuns(), pitcher.getInningsPitched());
            pitcher.calculateWHIP(pitcher.getWalks(), pitcher.getHits(), pitcher.getInningsPitched());
            pitcher.setPosition("P");
            pitcher.setContract("");
            
            
            // ADD IT TO THE COURSE
            phToLoad.addPitcher(pitcher);
            phToLoad.addPlayer(pitcher);
        }
        
    }
    

    @Override
    public PlayerHolder loadPlayerHolder(String filePath, String secondFilePath) throws IOException {
        PlayerHolder phToLoad = new PlayerHolder();
        loadPitchers(filePath, phToLoad);
        loadHitters(secondFilePath, phToLoad);
        return phToLoad;
    }
    
    
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    
    //MAKES AND RETURNS A JSON OBJECT FOR THE PROVIDED PITCHER
    private JsonObject makePitcherJsonObject(Pitcher pitcher) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_TEAM, pitcher.getTeam())
                                                   .add(JSON_LAST_NAME, pitcher.getLastName())
                                                   .add(JSON_FIRST_NAME, pitcher.getFirstName())
                                                   .add(JSON_IP, pitcher.getInningsPitched())
                                                   .add(JSON_ER, pitcher.getEarnedRuns())
                                                   .add(JSON_W, pitcher.getWins())
                                                   .add(JSON_SV, pitcher.getSaves())
                                                   .add(JSON_H, pitcher.getHits())
                                                   .add(JSON_BB, pitcher.getBasesOnBall())
                                                   .add(JSON_K, pitcher.getStrikeouts())
                                                   .add(JSON_NOTES, pitcher.getNotes())
                                                   .add(JSON_YEAR_OF_BIRTH, pitcher.getYearOfBirth())
                                                   .add(JSON_NATION_OF_BIRTH, pitcher.getNationOfBirth())
                                                   .add(JSON_POSITION, pitcher.getPlayingPosition())
                                                   .add(JSON_CONTRACT, pitcher.getContract())
                                                   .add(JSON_SALARY, pitcher.getSalary())
                                                   .add(JSON_FTEAM, pitcher.getFTeam())
                                                   .add(JSON_EST_VALUE, pitcher.getEstimatedValue())
                                                   .add(JSON_PICK_NUMBER, pitcher.getPickNumber())
                                                   .build();
        return jso;
    }
    
    private JsonObject makeHitterJsonObject(Hitter hitter) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_TEAM, hitter.getTeam())
                                                   .add(JSON_LAST_NAME, hitter.getLastName())
                                                   .add(JSON_FIRST_NAME, hitter.getFirstName())
                                                   .add(JSON_QP, hitter.getQualifiedPosition())
                                                   .add(JSON_AB, hitter.getAtBats())
                                                   .add(JSON_R, hitter.getRuns())
                                                   .add(JSON_H, hitter.getHits())
                                                   .add(JSON_HR, hitter.getHomeruns())
                                                   .add(JSON_RBI, hitter.getRunsBattedIn())
                                                   .add(JSON_SB, hitter.getStolenBases())
                                                   .add(JSON_NOTES, hitter.getNotes())
                                                   .add(JSON_YEAR_OF_BIRTH, hitter.getYearOfBirth())
                                                   .add(JSON_NATION_OF_BIRTH, hitter.getNationOfBirth())
                                                   .add(JSON_POSITION, hitter.getPlayingPosition())
                                                   .add(JSON_QUALIFIED_POSITION, hitter.getQualifiedPosition())
                                                   .add(JSON_CONTRACT, hitter.getContract())
                                                   .add(JSON_SALARY, hitter.getSalary())
                                                   .add(JSON_FTEAM, hitter.getFTeam())
                                                   .add(JSON_EST_VALUE, hitter.getEstimatedValue())
                                                   .add(JSON_PICK_NUMBER, hitter.getPickNumber())
                                                   .build();
        return jso;
    }
    
    private JsonObject makeFantasyTeamJsonObject(FantasyTeam team) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_TEAM_NAME, team.getTeamName())
                                                   .add(JSON_TEAM_OWNER, team.getTeamOwner())
                                                   .add(JSON_HITTERS, makeHittersJsonArray(team.getListOfActiveHitters()))
                                                   .add(JSON_PITCHERS, makePitchersJsonArray(team.getListOfActivePitchers()))
                                                   .add(JSON_CURRENT_QUOTA, makeCurrentQuotaJsonObject(team))
                                                   .add(JSON_TAXI_SQUAD, makeTaxiSquadJsonArray(team.getListOfTaxiPlayers()))
                                                   .build();
        return jso;
    }
    
    private JsonObject makeCurrentQuotaJsonObject (FantasyTeam team) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_CURRENT_C, team.getCurrentQuota().get("C"))
                                                   .add(JSON_CURRENT_1B, team.getCurrentQuota().get("1B"))
                                                   .add(JSON_CURRENT_CI, team.getCurrentQuota().get("CI"))
                                                   .add(JSON_CURRENT_3B, team.getCurrentQuota().get("3B"))
                                                   .add(JSON_CURRENT_2B, team.getCurrentQuota().get("2B"))
                                                   .add(JSON_CURRENT_MI, team.getCurrentQuota().get("MI"))
                                                   .add(JSON_CURRENT_SS, team.getCurrentQuota().get("SS"))
                                                   .add(JSON_CURRENT_U, team.getCurrentQuota().get("U"))
                                                   .add(JSON_CURRENT_OF, team.getCurrentQuota().get("OF"))
                                                   .add(JSON_CURRENT_P, team.getCurrentQuota().get("P"))
                                                   .build();
        return jso;
    }
    
    private JsonArray makeFantasyTeamsJsonArray(ObservableList<FantasyTeam> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (FantasyTeam fT : data) {
            jsb.add(makeFantasyTeamJsonObject(fT));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private JsonArray makePitchersJsonArray(ObservableList<Pitcher> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Pitcher p : data) {
            jsb.add(makePitcherJsonObject(p));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private JsonArray makeHittersJsonArray(ObservableList<Hitter> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Hitter h : data) {
            jsb.add(makeHitterJsonObject(h));
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    private JsonArray makeTaxiSquadJsonArray(ObservableList<Player> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player p : data) {
            if (p.getHitter()) {
                jsb.add(makeHitterJsonObject((Hitter)p));
            }
            else {
                jsb.add(makePitcherJsonObject((Pitcher)p));
            }
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    
}
