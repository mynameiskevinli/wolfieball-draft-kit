/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.file;

import wbdk.data.Player;
import wbdk.data.Pitcher;
import wbdk.data.Hitter;
import wbdk.data.FantasyTeam;
import wbdk.data.Draft;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import wbdk.data.PlayerHolder;
import wbdk.data.TeamHolder;
import wbdk.gui.WBDK_GUI;

/**
 *
 * @author Kevin
 */
public interface FileManager {
    public void                 saveDraft(WBDK_GUI gui, Draft draftToSave) throws IOException;
    public void                 loadDraft(Draft draftToLoad, String coursePath) throws IOException;
    public void                 loadHitters(String filePath, PlayerHolder phToLoad) throws IOException;
    public void                 loadPitchers(String filePath, PlayerHolder phToLoad) throws IOException;
    public PlayerHolder         loadPlayerHolder(String filePath, String secondFilePath) throws IOException;
//    public TeamHolder           loadTeamHolder(String filePath) throws IOException;
    
}
