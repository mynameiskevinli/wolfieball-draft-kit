/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk;

/**
 *
 * @author Kevin
 */
public enum WBDK_PropertyType {
    // LOADED FROM properties.xml
        PROP_APP_TITLE,
        
        // APPLICATION ICONS
        NEW_DRAFT_ICON,
        LOAD_DRAFT_ICON,
        SAVE_DRAFT_ICON,
        //VIEW_SCHEDULE_ICON,
        EXPORT_DRAFT_ICON,
        DELETE_ICON,
        EXIT_ICON,
        ADD_ICON,
        MINUS_ICON,
        FTEAM_ICON,
        PLAYER_ICON,
        DRAFT_ICON,
        MLB_ICON,
        FSTANDING_ICON,
        EDIT_ICON,
        PLAY_ICON,
        PAUSE_ICON,
        STAR_ICON,
        
        // APPLICATION TOOLTIPS FOR BUTTONS
        NEW_DRAFT_TOOLTIP,
        LOAD_DRAFT_TOOLTIP,
        SAVE_DRAFT_TOOLTIP,
        //VIEW_SCHEDULE_TOOLTIP,
        EXPORT_DRAFT_TOOLTIP,
        DELETE_TOOLTIP,
        EXIT_TOOLTIP,
        FTEAM_TOOLTIP,
        PLAYER_TOOLTIP,
        DRAFT_TOOLTIP,
        MLB_TOOLTIP,
        FSTANDING_TOOLTIP,
        ADD_PLAYER_TOOLTIP,
        REMOVE_PLAYER_TOOLTIP,
        ADD_TEAM_TOOLTIP,
        REMOVE_TEAM_TOOLTIP,
        PLAY_TOOLTIP,
        PAUSE_TOOLTIP,
        STAR_TOOLTIP,
        
        // FOR SCREEN EDIT WORKSPACE
        PLAYER_HEADING_LABEL,
        FTEAM_HEADING_LABEL,
        DRAFT_HEADING_LABEL,
        MLB_HEADING_LABEL,
        FSTANDING_HEADING_LABEL,
        PLAYER_SEARCH,
        NEW_DRAFT_NAME,
        STARTING_LINEUP,
        TAXI_LINEUP,
        SELECT_F_TEAM,
        SELECT_PRO_TEAM,
        
        // AND VERIFICATION MESSAGES
        NEW_DRAFT_CREATED_MESSAGE,
        DRAFT_LOADED_MESSAGE,
        DRAFT_SAVED_MESSAGE,
        SITE_EXPORTED_MESSAGE,
        SAVE_UNSAVED_WORK_MESSAGE,
        REMOVE_ITEM_MESSAGE
}
