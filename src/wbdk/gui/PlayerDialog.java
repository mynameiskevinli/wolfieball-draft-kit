/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;

import wbdk.WBDK_PropertyType;
import wbdk.data.Draft;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.data.PlayerHolder;
import wbdk.data.FantasyTeam;
import static wbdk.gui.WBDK_GUI.CLASS_HEADING_LABEL;
import static wbdk.gui.WBDK_GUI.CLASS_PROMPT_LABEL;
import static wbdk.gui.WBDK_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.Player;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import wbdk.data.TeamHolder;

/**
 *
 * @author Kevin
 */
public class PlayerDialog extends Stage{
    Player player;
    FantasyTeam freeAgent;
    
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Button completeButton;
    Button cancelButton;
    TextField salaryTextField;
    Label salaryLabel;
    ComboBox fTeamComboBox;
    Label fTeamLabel;
    ComboBox positionComboBox;
    Label positionLabel;
    ComboBox taxiPositionComboBox;
    ComboBox contractComboBox;
    Label contractLabel;
    ImageView image;
    VBox imageBox;
    VBox flagBox;
    ImageView flag;
    Label nameLabel;
    Label qpLabel;
    String qualPos;
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String NOTES_PROMPT = "Notes: ";
    public static final String PLAYER_HEADING = "Player Details";
    public static final String ADD_PLAYER_TITLE = "Add New Player";
    public static final String EDIT_PLAYER_TITLE = "Edit Player";
    public static final String FANTASY_TEAM_LABEL = "Fantasy Team: ";
    public static final String POSITION_LABEL = "Position: ";
    public static final String CONTRACT_LABEL = "Contract: ";
    public static final String SALARY_LABEL = "Salary ($): ";
    public static final String CONTRACT_S1 = "S1";
    public static final String CONTRACT_S2 = "S2";
    public static final String CONTRACT_X = "X";
    public static final String IMAGE_FILEPATH = "file:./images/players/";
    public static final String FLAG_FILEPATH = "file:./images/flags/";
    public static final String MISSING_IMAGE = "AAA_PhotoMissing";
    public static final String FREE_AGENT = "FREE AGENT";
    public static final String FIRST_NAME = "First Name:";
    public static final String LAST_NAME = "Last Name:";
    public static final String PRO_TEAM = "Pro Team:";
    public static final String ATL_TEAM = "ATL";
    public static final String AZ_TEAM = "AZ";
    public static final String CHC_TEAM = "CHC";
    public static final String CIN_TEAM = "CIN";
    public static final String COL_TEAM = "COL";
    public static final String LAD_TEAM = "LAD";
    public static final String MIA_TEAM = "MIA";
    public static final String MIL_TEAM = "MIL";
    public static final String NYM_TEAM = "NYM";
    public static final String PHI_TEAM = "PHI";
    public static final String PIT_TEAM = "PIT";
    public static final String SD_TEAM = "SD";
    public static final String SF_TEAM = "SF";
    public static final String STL_TEAM = "STL";
    public static final String WAS_TEAM = "WAS";
    
    ObservableList<String> listOfMLBTeams;
    
    public PlayerDialog(Stage primaryStage, PlayerHolder pH, MessageDialog messageDialog, TeamHolder tH) {
        
        listOfMLBTeams = FXCollections.observableArrayList();
        
        freeAgent = new FantasyTeam();
        freeAgent.setTeamName(FREE_AGENT);
        
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        contractComboBox = new ComboBox();
        
        headingLabel = new Label(PLAYER_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);

        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerDialog.this.selection = sourceButton.getText();
            PlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        completeButton.setDisable(true);
        
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(completeButton, 0, 10, 1, 1);
        gridPane.add(cancelButton, 1, 10, 1, 1);
        
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    public String getSelection() {
        return selection;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        positionComboBox.setValue(null);
        contractComboBox.setValue(null);
        fTeamComboBox.setValue(null);
        salaryTextField.setText("");
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditPlayerDialog(Player playerToEdit, TeamHolder tH) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_TITLE);
        
        fTeamLabel = new Label(FANTASY_TEAM_LABEL);
        fTeamComboBox = new ComboBox();
        
        player = new Player();
        
        loadPositions(playerToEdit, freeAgent);
        
        ObservableList<String> choice = FXCollections.observableArrayList();
        choice.clear();
        for (FantasyTeam s : tH.getTeams()) {
            choice.add(s.getTeamName());
        }
        //choice.add(freeAgent.getTeamName());
        fTeamComboBox.setItems(choice);
        fTeamComboBox.setOnAction(e -> {
            player.setFTeam(fTeamComboBox.getSelectionModel().getSelectedItem().toString());
            loadPositions(playerToEdit, getTeamFromName(player, tH));
            
            if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
                completeButton.setDisable(false);
            }
            if (getTeamFromName(player,tH).getListOfTaxiPlayers().size() == 8 && getTeamFromName(player,tH).calcPlayersNeeded() == 0) { //WILL THIS WORK?
                completeButton.setDisable(true);
//                System.out.println(getTeamFromName(player,tH).getTeamName() + " " + getTeamFromName(player,tH).getListOfTaxiPlayers().size());
            }
            
        });
        
        
        positionLabel = new Label(POSITION_LABEL);
        
        ObservableList<String> contracts = FXCollections.observableArrayList();
        contracts.add(CONTRACT_S1);
        contracts.add(CONTRACT_S2);
        contracts.add(CONTRACT_X);
        contractLabel = new Label (CONTRACT_LABEL);
//        contractComboBox = new ComboBox();
        contractComboBox.setItems(contracts);
        contractComboBox.setOnAction(e -> {
            player.setContract(contractComboBox.getSelectionModel().getSelectedItem().toString());
            if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
                completeButton.setDisable(false);
            }
            if (getTeamFromName(player,tH).getListOfTaxiPlayers().size() == 8 && getTeamFromName(player,tH).calcPlayersNeeded() == 0) { //WILL THIS WORK?
                completeButton.setDisable(true);
//                System.out.println(getTeamFromName(player,tH).getTeamName() + " " + getTeamFromName(player,tH).getListOfTaxiPlayers().size());
            }
        });
        
        salaryLabel = new Label(SALARY_LABEL);
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                player.setSalary(Integer.parseInt(newValue));
            } catch (Exception e) {
                
            }
            if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
                if (Integer.parseInt(salaryTextField.getText()) <= (getTeamFromComboBox(tH).getTotalBudget() + 1) - getTeamFromComboBox(tH).getPlayersNeeded()) {
                    completeButton.setDisable(false);
                }
                else {
                    completeButton.setDisable(true);
                }
//                completeButton.setDisable(false);
            }
            if (salaryTextField.getText().compareToIgnoreCase("") == 0) {
                completeButton.setDisable(true);
            }
            if (getTeamFromName(player,tH).getListOfTaxiPlayers().size() == 8 && getTeamFromName(player,tH).calcPlayersNeeded() == 0) { //WILL THIS WORK?
                completeButton.setDisable(true);
//                System.out.println(getTeamFromName(player,tH).getTeamName() + " " + getTeamFromName(player,tH).getListOfTaxiPlayers().size());
            }
        });
        
        imageBox = new VBox();
//        image = new ImageView(new Image(IMAGE_FILEPATH + MISSING_IMAGE + ".jpg"));
//        imageBox.getChildren().add(image);
        String imagePath = IMAGE_FILEPATH + playerToEdit.getLastName() + playerToEdit.getFirstName() + ".jpg";
        image = new ImageView(new Image(imagePath));
        if (image.getImage().getHeight() == 0) {
            image = new ImageView(new Image(IMAGE_FILEPATH + MISSING_IMAGE + ".jpg"));
        }
        imageBox.getChildren().removeAll(imageBox.getChildren());
        imageBox.getChildren().add(image);
        
        
        flagBox = new VBox();
        String flagPath = FLAG_FILEPATH + playerToEdit.getNationOfBirth() + ".png";
        flag = new ImageView(new Image(flagPath));
        nameLabel = new Label(playerToEdit.getFirstName() + " " + playerToEdit.getLastName());
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        if (playerToEdit.getHitter() == true) {
            qpLabel = new Label(((Hitter)playerToEdit).getQualifiedPosition());
            qpLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        }
        else {
            qpLabel = new Label(((Pitcher)playerToEdit).getPosition());
            qpLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        }
        flagBox.getChildren().removeAll(flagBox.getChildren());
        flagBox.getChildren().add(flag);
        flagBox.getChildren().add(nameLabel);
        flagBox.getChildren().add(qpLabel);
        
        gridPane.add(flagBox, 1, 1, 1, 1);
        gridPane.add(imageBox, 0, 1, 1, 1);
        gridPane.add(fTeamLabel, 0, 5, 1, 1);
        gridPane.add(fTeamComboBox, 1, 5, 1, 1);
        gridPane.add(positionLabel, 0, 6, 1, 1);
        gridPane.add(contractLabel, 0, 7, 1, 1);
        gridPane.add(contractComboBox, 1, 7, 1, 1);
        gridPane.add(salaryLabel, 0, 8, 1, 1);
        gridPane.add(salaryTextField, 1, 8, 1, 1);
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public void showEditPlayerDialog2(Player playerToEdit, TeamHolder tH) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_TITLE);
        
        fTeamLabel = new Label(FANTASY_TEAM_LABEL);
        fTeamComboBox = new ComboBox();
        
        player = new Player();
        
        loadPositions(playerToEdit, freeAgent);
        
        ObservableList<String> choice = FXCollections.observableArrayList();
        choice.clear();
        for (FantasyTeam s : tH.getTeams()) {
            choice.add(s.getTeamName());
        }
        choice.add(freeAgent.getTeamName());
        fTeamComboBox.setItems(choice);
        fTeamComboBox.setOnAction(e -> {
            player.setFTeam(fTeamComboBox.getSelectionModel().getSelectedItem().toString());
            loadPositions(playerToEdit, getTeamFromName(player, tH));
            
            if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
                completeButton.setDisable(false);
            }
            if (getTeamFromName(player,tH).getListOfTaxiPlayers().size() == 8 && getTeamFromName(player,tH).calcPlayersNeeded() == 0) { //WILL THIS WORK?
                completeButton.setDisable(true);
            }
        });
        
        
        positionLabel = new Label(POSITION_LABEL);
        
        ObservableList<String> contracts = FXCollections.observableArrayList();
        contracts.add(CONTRACT_S1);
        contracts.add(CONTRACT_S2);
        contracts.add(CONTRACT_X);
        contractLabel = new Label (CONTRACT_LABEL);
//        contractComboBox = new ComboBox();
        contractComboBox.setItems(contracts);
        contractComboBox.setOnAction(e -> {
            player.setContract(contractComboBox.getSelectionModel().getSelectedItem().toString());
            if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
                completeButton.setDisable(false);
            }
            if (getTeamFromName(player,tH).getListOfTaxiPlayers().size() == 8 && getTeamFromName(player,tH).calcPlayersNeeded() == 0) { //WILL THIS WORK?
                completeButton.setDisable(true);
            }
        });
        
        salaryLabel = new Label(SALARY_LABEL);
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setSalary(Integer.parseInt(newValue));
            if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
//                completeButton.setDisable(false);
                if (Integer.parseInt(salaryTextField.getText()) <= (getTeamFromComboBox(tH).getTotalBudget() + 1) - getTeamFromComboBox(tH).getPlayersNeeded()) {
                    completeButton.setDisable(false);
                }
                else {
                    completeButton.setDisable(true);
                }
            }
            if (salaryTextField.getText().compareToIgnoreCase("") == 0) {
                completeButton.setDisable(true);
            }
            if (getTeamFromName(player,tH).getListOfTaxiPlayers().size() == 8 && getTeamFromName(player,tH).calcPlayersNeeded() == 0) { //WILL THIS WORK?
                completeButton.setDisable(true);
            }
        });
        
  
        
        imageBox = new VBox();
//        image = new ImageView(new Image(IMAGE_FILEPATH + MISSING_IMAGE + ".jpg"));
//        imageBox.getChildren().add(image);
        String imagePath = IMAGE_FILEPATH + playerToEdit.getLastName() + playerToEdit.getFirstName() + ".jpg";
        image = new ImageView(new Image(imagePath));
        if (image.getImage().getHeight() == 0) {
            image = new ImageView(new Image(IMAGE_FILEPATH + MISSING_IMAGE + ".jpg"));
        }
        imageBox.getChildren().removeAll(imageBox.getChildren());
        imageBox.getChildren().add(image);
        
        
        flagBox = new VBox();
        String flagPath = FLAG_FILEPATH + playerToEdit.getNationOfBirth() + ".png";
        flag = new ImageView(new Image(flagPath));
        nameLabel = new Label(playerToEdit.getFirstName() + " " + playerToEdit.getLastName());
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        if (playerToEdit.getHitter() == true) {
            qpLabel = new Label(((Hitter)playerToEdit).getQualifiedPosition());
            qpLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        }
        else {
            qpLabel = new Label(((Pitcher)playerToEdit).getPosition());
            qpLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        }
        flagBox.getChildren().removeAll(flagBox.getChildren());
        flagBox.getChildren().add(flag);
        flagBox.getChildren().add(nameLabel);
        flagBox.getChildren().add(qpLabel);
        
        gridPane.add(flagBox, 1, 1, 1, 1);
        gridPane.add(imageBox, 0, 1, 1, 1);
        gridPane.add(fTeamLabel, 0, 5, 1, 1);
        gridPane.add(fTeamComboBox, 1, 5, 1, 1);
        gridPane.add(positionLabel, 0, 6, 1, 1);
        gridPane.add(contractLabel, 0, 7, 1, 1);
        gridPane.add(contractComboBox, 1, 7, 1, 1);
        gridPane.add(salaryLabel, 0, 8, 1, 1);
        gridPane.add(salaryTextField, 1, 8, 1, 1);
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public void showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_PLAYER_TITLE);
        
        player = new Player();
        
        Label firstNameLabel = new Label(FIRST_NAME);
        TextField firstNameTextField = new TextField();
        Label lastNameLabel = new Label(LAST_NAME);
        TextField lastNameTextField = new TextField();
        Label proTeamLabel = new Label(PRO_TEAM);
        ComboBox proTeamComboBox = new ComboBox();
        
        CheckBox checkBoxC = new CheckBox("C");
        CheckBox checkBox1B = new CheckBox("1B");
        CheckBox checkBox3B = new CheckBox("3B");
        CheckBox checkBox2B = new CheckBox("2B");
        CheckBox checkBoxSS = new CheckBox("SS");
        CheckBox checkBoxOF = new CheckBox("OF");
        CheckBox checkBoxP = new CheckBox("P");
        HBox checkBoxHolder = new HBox();
        checkBoxHolder.getChildren().add(checkBoxC);
        checkBoxHolder.getChildren().add(checkBox1B);
        checkBoxHolder.getChildren().add(checkBox3B);
        checkBoxHolder.getChildren().add(checkBox2B);
        checkBoxHolder.getChildren().add(checkBoxSS);
        checkBoxHolder.getChildren().add(checkBoxOF);
        checkBoxHolder.getChildren().add(checkBoxP);
        
        gridPane.add(firstNameLabel, 0, 1, 1, 1);
        gridPane.add(firstNameTextField, 1, 1, 1, 1);
        gridPane.add(lastNameLabel, 0, 2, 1, 1);
        gridPane.add(lastNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(proTeamComboBox, 1, 3, 1, 1);
        gridPane.add(checkBoxHolder, 1, 4, 1, 1);

        qualPos = new String();
        
        // IF A HITTER CHECKBOX IS SELECTED, DISABLE THE PITCHER CHECKBOX
        checkBoxC.setOnAction(e -> {
            if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected()) {
                checkBoxP.setDisable(true);
                qualPos += "C_";
                player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
            }
            else {
                checkBoxP.setDisable(false);
                qualPos = qualPos.replaceAll("C_", "");
                if (qualPos.isEmpty()) {
                    player.setPlayingPosition("");
                }
                else {
                    player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
                }
            }
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (!checkBoxC.isSelected() && !checkBox1B.isSelected() && !checkBox3B.isSelected() && !checkBox2B.isSelected()
                    && !checkBoxSS.isSelected() && !checkBoxOF.isSelected() && !checkBoxP.isSelected()) {
                completeButton.setDisable(true);
            }
        });
        
        checkBox1B.setOnAction(e -> {
            if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected()) {
                checkBoxP.setDisable(true);
                if (checkBox3B.isSelected()) {
                    qualPos += "1B_";
                }
                else {
                    qualPos += "1B_CI_";
                }
                player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
            }
            else {
                checkBoxP.setDisable(false);
                if (checkBox3B.isSelected()) {
                    qualPos = qualPos.replaceAll("1B_", "");
                }
                else {
                    qualPos = qualPos.replaceAll("1B_CI_", "");
                }
                if (qualPos.isEmpty()) {
                    player.setPlayingPosition("");
                }
                else {
                    player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
                }
            }
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (!checkBoxC.isSelected() && !checkBox1B.isSelected() && !checkBox3B.isSelected() && !checkBox2B.isSelected()
                    && !checkBoxSS.isSelected() && !checkBoxOF.isSelected() && !checkBoxP.isSelected()) {
                completeButton.setDisable(true);
            }
        });
        
        checkBox3B.setOnAction(e -> {
            if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected()) {
                checkBoxP.setDisable(true);
                if (checkBox1B.isSelected()) {
                    qualPos += "3B_";
                }
                else {
                    qualPos += "3B_CI_";
                }
                player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
            }
            else {
                checkBoxP.setDisable(false);
                if (checkBox1B.isSelected()) {
                    qualPos = qualPos.replaceAll("3B_", "");
                }
                else {
                    qualPos = qualPos.replaceAll("3B_CI_", "");
                }
                if (qualPos.isEmpty()) {
                    player.setPlayingPosition("");
                }
                else {
                    player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
                }
            }
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (!checkBoxC.isSelected() && !checkBox1B.isSelected() && !checkBox3B.isSelected() && !checkBox2B.isSelected()
                    && !checkBoxSS.isSelected() && !checkBoxOF.isSelected() && !checkBoxP.isSelected()) {
                completeButton.setDisable(true);
            }
        });
        
        checkBox2B.setOnAction(e -> {
            if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected()) {
                checkBoxP.setDisable(true);
                if (checkBoxSS.isSelected()) {
                    qualPos += "2B_";
                }
                else {
                    qualPos += "2B_MI_";
                }
                player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
            }
            else {
                checkBoxP.setDisable(false);
                if (checkBoxSS.isSelected()) {
                    qualPos = qualPos.replaceAll("2B_", "");
                }
                else {
                    qualPos = qualPos.replaceAll("2B_MI_", "");
                }
                if (qualPos.isEmpty()) {
                    player.setPlayingPosition("");
                }
                else {
                    player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
                }
            }
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (!checkBoxC.isSelected() && !checkBox1B.isSelected() && !checkBox3B.isSelected() && !checkBox2B.isSelected()
                    && !checkBoxSS.isSelected() && !checkBoxOF.isSelected() && !checkBoxP.isSelected()) {
                completeButton.setDisable(true);
            }
        });
        
        checkBoxSS.setOnAction(e -> {
            if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected()) {
                checkBoxP.setDisable(true);
                if (checkBox2B.isSelected()) {
                    qualPos += "SS_";
                }
                else {
                    qualPos += "SS_MI_";
                }
                player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
            }
            else {
                checkBoxP.setDisable(false);
                if (checkBox2B.isSelected()) {
                    qualPos = qualPos.replaceAll("SS_", "");
                }
                else {
                    qualPos = qualPos.replaceAll("SS_MI_", "");
                }
                if (qualPos.isEmpty()) {
                    player.setPlayingPosition("");
                }
                else {
                    player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
                }
            }
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (!checkBoxC.isSelected() && !checkBox1B.isSelected() && !checkBox3B.isSelected() && !checkBox2B.isSelected()
                    && !checkBoxSS.isSelected() && !checkBoxOF.isSelected() && !checkBoxP.isSelected()) {
                completeButton.setDisable(true);
            }
        });
        
        checkBoxOF.setOnAction(e -> {
            if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected()) {
                checkBoxP.setDisable(true);
                qualPos += "OF_";
                player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
            }
            else {
                checkBoxP.setDisable(false);
                qualPos = qualPos.replaceAll("OF_", "");
                if (qualPos.isEmpty()) {
                    player.setPlayingPosition("");
                }
                else {
                    player.setPlayingPosition(qualPos.substring(0, qualPos.length()-1));
                }
            }
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (!checkBoxC.isSelected() && !checkBox1B.isSelected() && !checkBox3B.isSelected() && !checkBox2B.isSelected()
                    && !checkBoxSS.isSelected() && !checkBoxOF.isSelected() && !checkBoxP.isSelected()) {
                completeButton.setDisable(true);
            }
        });
        
        checkBoxP.setOnAction(e -> {
            if (checkBoxP.isSelected()) {
                checkBoxC.setDisable(true);
                checkBox1B.setDisable(true);
                checkBox3B.setDisable(true);
                checkBox2B.setDisable(true);
                checkBoxSS.setDisable(true);
                checkBoxOF.setDisable(true);
                player.setPlayingPosition("P");
            }
            else {
                checkBoxC.setDisable(false);
                checkBox1B.setDisable(false);
                checkBox3B.setDisable(false);
                checkBox2B.setDisable(false);
                checkBoxSS.setDisable(false);
                checkBoxOF.setDisable(false);
                player.setPlayingPosition("");
            }
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (!checkBoxC.isSelected() && !checkBox1B.isSelected() && !checkBox3B.isSelected() && !checkBox2B.isSelected()
                    && !checkBoxSS.isSelected() && !checkBoxOF.isSelected() && !checkBoxP.isSelected()) {
                completeButton.setDisable(true);
            }
        });
        
        listOfMLBTeams.add(ATL_TEAM);
        listOfMLBTeams.add(AZ_TEAM);
        listOfMLBTeams.add(CHC_TEAM);
        listOfMLBTeams.add(CIN_TEAM);
        listOfMLBTeams.add(COL_TEAM);
        listOfMLBTeams.add(LAD_TEAM);
        listOfMLBTeams.add(MIA_TEAM);
        listOfMLBTeams.add(MIL_TEAM);
        listOfMLBTeams.add(NYM_TEAM);
        listOfMLBTeams.add(PHI_TEAM);
        listOfMLBTeams.add(PIT_TEAM);
        listOfMLBTeams.add(SD_TEAM);
        listOfMLBTeams.add(SF_TEAM);
        listOfMLBTeams.add(STL_TEAM);
        listOfMLBTeams.add(WAS_TEAM);
        proTeamComboBox.setItems(listOfMLBTeams);
        
        proTeamComboBox.setOnAction(e -> {
            player.setTeam(proTeamComboBox.getSelectionModel().getSelectedItem().toString());
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
        });
        
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(newValue);
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (firstNameTextField.getText().compareToIgnoreCase("") == 0) {
                completeButton.setDisable(true);
            }
        });
        
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
            if (proTeamComboBox.getSelectionModel().getSelectedItem() != null && firstNameTextField.getText().compareToIgnoreCase("") != 0 && lastNameTextField.getText().compareToIgnoreCase("") != 0) {
                if (checkBoxC.isSelected() || checkBox1B.isSelected() || checkBox3B.isSelected() || checkBox2B.isSelected() || checkBoxSS.isSelected() || checkBoxOF.isSelected() || checkBoxP.isSelected()) {
                    completeButton.setDisable(false);
                }
            }
            if (lastNameTextField.getText().compareToIgnoreCase("") == 0) {
                completeButton.setDisable(true);
            }
        });
        
        
        
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public void loadPositions(Player playerToEdit, FantasyTeam team) {
        taxiPositionComboBox = new ComboBox();
        positionComboBox = new ComboBox();
        ObservableList<String> positions = FXCollections.observableArrayList();
        positions.clear();
        if (playerToEdit.getHitter() == true) {
            String[] posList = ((Hitter)playerToEdit).getQPList();
            for (int i = 0; i < posList.length; i++) {
                if (team.getCurrentQuota().get(posList[i]) < team.getMaxQuota().get(posList[i])) {
                    positions.add(posList[i]);
                }
            }
        }
        else {
            if (team.getCurrentQuota().get("P") < team.getMaxQuota().get("P")) {
                positions.add(((Pitcher)playerToEdit).getPosition());
            }
        }
            
        if (team.getPlayersNeeded() == 0) {
            ObservableList<String> allPos = FXCollections.observableArrayList();
            allPos.clear();
            if (playerToEdit.getHitter()) {
                String[] hitList = ((Hitter)playerToEdit).getQPList();
                for (int i = 0; i < hitList.length; i++) {
                    allPos.add(hitList[i]);
                }
            }
            else {
                allPos.add(((Pitcher)playerToEdit).getPosition());
            }
//            taxiPositionComboBox = new ComboBox();
            gridPane.add(taxiPositionComboBox, 1, 6, 1, 1);
            taxiPositionComboBox.setItems(allPos);
            taxiPositionComboBox.setOnAction(e -> {
                player.setPlayingPosition(taxiPositionComboBox.getSelectionModel().getSelectedItem().toString());
                if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
                    completeButton.setDisable(false);
                }
                if (team.getListOfTaxiPlayers().size() == 8 && team.calcPlayersNeeded() == 0) { //WILL THIS WORK?
                    completeButton.setDisable(true);
                }
            });
            ObservableList<String> contractX = FXCollections.observableArrayList();
            contractX.clear();
            contractX.add(CONTRACT_X);
            contractComboBox.setItems(contractX);
            //NEEDS FIXINGGGGGGGGGG
            
        }
        else {
//            positionComboBox = new ComboBox();
            gridPane.add(positionComboBox, 1, 6, 1, 1);
            positionComboBox.setItems(positions);
            positionComboBox.setOnAction(e -> {
                player.setPlayingPosition(positionComboBox.getSelectionModel().getSelectedItem().toString());
                if (fTeamComboBox.getSelectionModel().getSelectedItem() != null && (taxiPositionComboBox.getSelectionModel().getSelectedItem() != null || positionComboBox.getSelectionModel().getSelectedItem() != null) 
                        && contractComboBox.getSelectionModel().getSelectedItem() != null && salaryTextField.getText().compareToIgnoreCase("") != 0) {
                    completeButton.setDisable(false);
                }
            });
            ObservableList<String> contracts = FXCollections.observableArrayList();
            contracts.clear();
            contracts.add(CONTRACT_S1);
            contracts.add(CONTRACT_S2);
            contracts.add(CONTRACT_X);
            contractComboBox.setItems(contracts);
        }
    }
    
    public FantasyTeam getTeamFromName(Player player, TeamHolder tH) {
        FantasyTeam team = freeAgent;
        for (int i = 0; i < tH.getTeams().size(); i++) {
            if (player.getFTeam().compareToIgnoreCase(tH.getTeams().get(i).getTeamName()) == 0) {
                team = tH.getTeams().get(i);
            }
        }
        return team;
    }
    
    public FantasyTeam getTeamFromComboBox(TeamHolder tH) {
        FantasyTeam team = freeAgent;
        for (int i = 0; i < tH.getTeams().size(); i++) {
            if (((String)fTeamComboBox.getSelectionModel().getSelectedItem()).compareToIgnoreCase(tH.getTeams().get(i).getTeamName()) == 0) {
                team = tH.getTeams().get(i);
            }
        }
        return team;
    }
}
