/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;

import wbdk.WBDK_PropertyType;
import wbdk.data.Draft;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.data.PlayerHolder;
import wbdk.data.FantasyTeam;
import static wbdk.gui.WBDK_GUI.CLASS_HEADING_LABEL;
import static wbdk.gui.WBDK_GUI.CLASS_PROMPT_LABEL;
import static wbdk.gui.WBDK_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.Player;
import wbdk.data.TeamHolder;

/**
 *
 * @author Kevin
 */
public class FantasyTeamDialog extends Stage{
    FantasyTeam team;
    
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    TextField nameTextField;
    Label ownerLabel;
    TextField ownerTextField;
    Button completeButton;
    Button cancelButton;
    
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String NAME_PROMPT = "Name: ";
    public static final String OWNER_PROMPT = "Owner: ";
    public static final String FANTASY_TEAM_HEADING = "Fantasy Team Details";
    public static final String ADD_FANTASY_TEAM_TITLE = "Add New Fantasy Team";
    public static final String EDIT_FANTASY_TEAM_TITLE = "Edit Fantasy Team";
    
    public FantasyTeamDialog(Stage primaryStage, TeamHolder tH, MessageDialog messageDialog) {
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        headingLabel = new Label(FANTASY_TEAM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        nameLabel = new Label(NAME_PROMPT);
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (nameTextField.getText().compareToIgnoreCase("") != 0 && ownerTextField.getText().compareToIgnoreCase("") != 0) {
                completeButton.setDisable(false);
            }
            if (nameTextField.getText().compareToIgnoreCase("") == 0) {
                completeButton.setDisable(true);
            }
            for (int i = 0; i < tH.getTeams().size(); i++) {
                if (nameTextField.getText().compareToIgnoreCase(tH.getTeams().get(i).getTeamName()) == 0) {
                    completeButton.setDisable(true);
                }
            }
            team.setTeamName(newValue);
        });
        
        ownerLabel = new Label(OWNER_PROMPT);
        ownerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        ownerTextField = new TextField();
        ownerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (nameTextField.getText().compareToIgnoreCase("") != 0 && ownerTextField.getText().compareToIgnoreCase("") != 0) {
                completeButton.setDisable(false);
            }
            if (ownerTextField.getText().compareToIgnoreCase("") == 0) {
                completeButton.setDisable(true);
            }
            for (int i = 0; i < tH.getTeams().size(); i++) {
                if (nameTextField.getText().compareToIgnoreCase(tH.getTeams().get(i).getTeamName()) == 0) {
                    completeButton.setDisable(true);
                }
            }
            team.setTeamOwner(newValue);
        });
        
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        completeButton.setDisable(true);
        
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            FantasyTeamDialog.this.selection = sourceButton.getText();
            FantasyTeamDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(nameLabel, 0, 1, 1 ,1);
        gridPane.add(nameTextField, 1, 1, 1, 1);
        gridPane.add(ownerLabel, 0, 2, 1 ,1);
        gridPane.add(ownerTextField, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 4, 1, 1);
        gridPane.add(cancelButton, 1, 4, 1, 1);
        
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    public String getSelection() {
        return selection;
    }
    
    public FantasyTeam getTeam() {
        return team;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        nameTextField.setText(team.getTeamName());
        ownerTextField.setText(team.getTeamOwner());
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditTeamDialog(FantasyTeam teamToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_FANTASY_TEAM_TITLE);
        
        team = new FantasyTeam();
        team.setTeamName(teamToEdit.getTeamName());
        team.setTeamOwner(teamToEdit.getTeamOwner());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public void showAddTeamDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_FANTASY_TEAM_TITLE);
        
        team = new FantasyTeam();
        team.setTeamName("");
        team.setTeamOwner("");
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
}
