/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.controller.FileController;
import wbdk.controller.PlayerController;
import wbdk.data.DataManager;
import wbdk.data.DataView;
import wbdk.data.Draft;
import wbdk.data.FantasyTeam;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.data.Player;
import wbdk.file.FileManager;
import wbdk.file.JsonFileManager;
import wbdk.WBDK_PropertyType;
import static wbdk.WBDK_StartupConstants.CLOSE_BUTTON_LABEL;
import static wbdk.WBDK_StartupConstants.PATH_CSS;
import static wbdk.WBDK_StartupConstants.PATH_IMAGES;
import wbdk.controller.FantasyTeamController;
import wbdk.data.TeamHolder;


/**
 *
 * @author Kevin
 */
public class WBDK_GUI implements DataView{
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wbdk_style.css";
    static final String CLASS_BORDERED_PANE_BLUE = "bordered_pane";
    static final String CLASS_BORDERED_PANE_GREEN = "bordered_pane2";
    static final String CLASS_BORDERED_PANE_PURPLE = "bordered_pane3";
    static final String CLASS_BORDERED_PANE_GREY = "bordered_pane4";
    static final String CLASS_BORDERED_PANE_RED = "bordered_pane5";
    static final String CLASS_BORDERED_PANE_TEAL = "bordered_pane6";
    static final String CLASS_BORDERED_PANE_BACK = "bordered_pane7";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    
    
    static final String RADIO_ALL = "All";
    static final String RADIO_C = "C";
    static final String RADIO_1B = "1B";
    static final String RADIO_CI = "CI";
    static final String RADIO_3B = "3B";
    static final String RADIO_2B = "2B";
    static final String RADIO_MI = "MI";
    static final String RADIO_SS = "SS";
    static final String RADIO_OF = "OF";
    static final String RADIO_U = "U";
    static final String RADIO_P = "P";

    // THIS MANAGES ALL OF THE APPLICATION'S DATA
    DataManager dataManager;

    // THIS MANAGES COURSE FILE I/O
    FileManager fileManager;

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;
    
    // THIS HANDLES REQUESTS TO ADD OR EDIT PLAYER STUFF
    PlayerController playerController;
    
    //THIS HANDLES REQUEST TO ADD OR EDIT FANTASY TEAM STUFF
    FantasyTeamController teamController;

    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane wbdkPane;
    
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exitButton;
    
    // THIS IS THE BOTTOM TOOLBAR AND ITS CONTROLS
    FlowPane bottomToolbarPane;
    Button fantasyTeamButton;
    Button playerButton;
    Button draftButton;
    Button mlbButton;
    Button fantasyStandingButton;

    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
    BorderPane workspacePane;
    boolean workspaceActivated;
    
    // WE'LL PUT THE WORKSPACE INSIDE A SCROLL PANE
    ScrollPane workspaceScrollPane;

    // WE'LL BE USING THIS AS OUR MAIN SCREEN
    VBox screensPane;
    Label headingLabel;
    
    // WE'LL BE USING THIS FOR OUR PLAYERS SCREEN
    VBox playersPane;
    HBox playerInfoPane;
    BorderPane playersControlPane;
    HBox playersControlButtonsPane;
    HBox playersSearchPane;
    Button addPlayerButton;
    Button removePlayerButton;
    Label searchPlayerLabel;
    TextField searchPlayerTextField;
    //VBox playerTopPane;
    FilteredList<Player> filteredPlayers;
    HBox playersRadioPane;
    RadioButton allButton;
    RadioButton cButton;
    RadioButton oneB_Button;
    RadioButton ciButton;
    RadioButton threeB_Button;
    RadioButton twoB_Button;
    RadioButton miButton;
    RadioButton ssButton;
    RadioButton ofButton;
    RadioButton uButton;
    RadioButton pButton;
    ToggleGroup radioToggle;
    
    //THIS REGION IS FOR THE PLAYERS TABLE
    HBox playerBox;
    TableView<Player> playerTable;
    TableColumn firstColumn;
    TableColumn lastColumn;
    TableColumn proTeamColumn;
    TableColumn positionsColumn;
    TableColumn yearOfBirthColumn;
    TableColumn r_w_Column;
    TableColumn hr_sv_Column;
    TableColumn rbi_k_Column;
    TableColumn sb_era_Column;
    TableColumn ba_whip_Column;
    TableColumn estimatedValueColumn;
    TableColumn notesColumn;
    TableColumn randomColumn;
    
    
    // AND TABLE COLUMNS
    static final String COL_FIRST = "First";
    static final String COL_LAST = "Last";
    static final String COL_PRO_TEAM = "Pro Team";
    static final String COL_POSITIONS = "Positions";
    static final String COL_YEAR_OF_BIRTH = "Year Of Birth";
    static final String COL_R_W = "R/W";
    static final String COL_HR_SV = "HR/SV";
    static final String COL_RBI_K = "RBI/K";
    static final String COL_SB_ERA = "SB/ERA";
    static final String COL_BA_WHIP = "BA/WHIP";
    static final String COL_ESTIMATED_VALUE = "Estimated Value";
    static final String COL_NOTES = "Notes";
    static final String COL_R = "R";
    static final String COL_HR = "HR";
    static final String COL_RBI = "RBI";
    static final String COL_SB = "SB";
    static final String COL_BA = "BA";
    static final String COL_W = "W";
    static final String COL_SV = "SV";
    static final String COL_K = "K";
    static final String COL_ERA = "ERA";
    static final String COL_WHIP = "WHIP";
    static final String COL_SALARY = "Salary";
    static final String COL_CONTRACT = "Contract";
    static final String COL_POSITION = "Position";
    static final String COL_FTEAM = "Fantasy Team";
    static final String COL_PLAYERS_NEEDED = "Players Needed";
    static final String COL_BUDGET_LEFT = "Budget Left";
    static final String COL_PRICE_PER_PLAYER = "Price per player";
    static final String COL_TOTAL_POINTS = "Total Points";
    static final String COL_PICK = "Picked #";
    
    
    // FANTASY TEAM SCREEN GUI
    VBox fantasyTeamPane;
    HBox draftBarPane;
    Label draftNameLabel;
    TextField draftNameTextField;
    
    // BUTTONS AND CONTROLS
    VBox fantasyTeamControlPane;
    HBox fantasyTeamButtonPane;
    Button addTeamButton;
    Button removeTeamButton;
    Button editTeamButton;
    HBox selectFTeamComboPane;
    Label selectFTeamLabel;
    ComboBox fantasyTeamComboBox;
    
    // FANTASY TEAM TABLE
    VBox fantasyTeamTablePane;
    Label fantasyTeamTableLabel;
    TableView<Player> fantasyTeamsTable;
    TableColumn ftPositionColumn;
    TableColumn ftFirstColumn;
    TableColumn ftLastColumn;
    TableColumn ftProColumn;
    TableColumn ftPositionsColumn;
    TableColumn ft_R_W_Column;
    TableColumn ft_HR_SV_Column;
    TableColumn ft_RBI_K_Column;
    TableColumn ft_SB_ERA_Column;
    TableColumn ft_BA_WHIP_Column;
    TableColumn ft_Value_Column;
    TableColumn ft_Contract_Column;
    TableColumn ft_Salary_Column;
    
    // TAXI TEAM TABLE
    VBox taxiTeamTablePane;
    Label taxiTeamTableLabel;
    TableView<Player> taxiTeamsTable;
    TableColumn taxiPositionColumn;
    TableColumn taxiFirstColumn;
    TableColumn taxiLastColumn;
    TableColumn taxiProColumn;
    TableColumn taxiPositionsColumn;
    TableColumn taxi_R_W_Column;
    TableColumn taxi_HR_SV_Column;
    TableColumn taxi_RBI_K_Column;
    TableColumn taxi_SB_ERA_Column;
    TableColumn taxi_BA_WHIP_Column;
    TableColumn taxi_Value_Column;
    TableColumn taxi_Contract_Column;
    TableColumn taxi_Salary_Column;
    
    // MLB TEAM PAGE
    ComboBox<String> mlbComboBox;
    Label mlbComboBoxLabel;
    VBox mlbTeamPane;
    HBox mlbControlsPane;
    VBox mlbTeamTablePane;
    TableView<Player> mlbTeamsTable;
    TableColumn mlbProTeamColumn;
    TableColumn mlbLastNameColumn;
    TableColumn mlbFirstNameColumn;
    TableColumn mlbQualifiedPositionsColumn;
    
    // LIST OF MLB TEAMS
    static final String MLB_ATL = "ATL";
    static final String MLB_AZ = "AZ";
    static final String MLB_CHC = "CHC";
    static final String MLB_CIN = "CIN";
    static final String MLB_COL = "COL";
    static final String MLB_LAD = "LAD";
    static final String MLB_MIA = "MIA";
    static final String MLB_MIL = "MIL";
    static final String MLB_NYM = "NYM";
    static final String MLB_PHI = "PHI";
    static final String MLB_PIT = "PIT";
    static final String MLB_SD = "SD";
    static final String MLB_SF = "SF";
    static final String MLB_STL = "STL";
    static final String MLB_WAS = "WAS";
    
    // FANATASY STANDINGS PAGE
    VBox fantasyStandingsPane;
    VBox standingsTablePane;
    TableView<FantasyTeam> standingsTable;
    TableColumn standingsTeamColumn;
    TableColumn standingsPlayerNeededColumn;
    TableColumn standingsMoneyLeftColumn;
    TableColumn standingsMoneyPerPlayerColumn;
    TableColumn standingsRColumn;
    TableColumn standingsHRColumn;
    TableColumn standingsRBIColumn;
    TableColumn standingsSBColumn;
    TableColumn standingsBAColumn;
    TableColumn standingsWColumn;
    TableColumn standingsSVColumn;
    TableColumn standingsKColumn;
    TableColumn standingsERAColumn;
    TableColumn standingsWHIPColumn;
    TableColumn standingsTotalPointsColumn;
    
    // DRAFT PAGE
    VBox draftPane;
    VBox draftTablePane;
    HBox draftControlsPane;
    TableView<Player> draftTable;
    TableColumn draftPick;
    TableColumn draftFirstName;
    TableColumn draftLastName;
    TableColumn draftFTeamName;
    TableColumn draftContract;
    TableColumn draftSalary;
    Button starDraft;
    Button playDraft;
    Button pauseDraft;
    
    // HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    ObservableList<Hitter> displayHitters;
    ObservableList<Pitcher> displayPitchers;
    ObservableList<Player> displayPlayers;
    ObservableList<FantasyTeam> displayTeams;
    ObservableList<Player> emptyPlayers;
    ObservableList<String> mlbDisplayTeams;
    ObservableList<Player> displayMLB;
    ObservableList<Player> draftedPlayers;
    
    
    public WBDK_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
        displayHitters = FXCollections.observableArrayList();
        displayPitchers = FXCollections.observableArrayList();
        displayPlayers = FXCollections.observableArrayList();
        filteredPlayers = new FilteredList<Player>(displayPlayers, p->true); 
        displayTeams = FXCollections.observableArrayList();
        emptyPlayers = FXCollections.observableArrayList();
        mlbDisplayTeams = FXCollections.observableArrayList();
        displayMLB = FXCollections.observableArrayList();
        draftedPlayers = FXCollections.observableArrayList();
    }
    
    public DataManager getDataManager() {
        return dataManager;
    }
    
    public FileController getFileController() {
        return fileController;
    }
    
    public FileManager getFileManager() {
        return fileManager;
    }
    
    public Stage getWindow() {
        return primaryStage;
    }
    
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }
    
    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }
    
    public void setDataManager(DataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    public void setFileManager(FileManager initCourseFileManager) {
        fileManager = initCourseFileManager;
    }
    
    public TableView<Player> getPlayerTable() {
        return playerTable;
    }
    
    public TableView<Player> getFTeamTable() {
        return fantasyTeamsTable;
    }
    
    public ObservableList<Player> getDisplayPlayers() {
        return displayPlayers;
    }
    
    public ObservableList<Hitter> getDisplayHitters() {
        return displayHitters;
    }
    
    public ObservableList<Pitcher> getDisplayPitchers() {
        return displayPitchers;
    }
    
    public ObservableList<Player> getDisplayMLB() {
        return displayMLB;
    }
    
    public ObservableList<Player> getDraftedPlayers() {
        return draftedPlayers;
    }
    
    public void fillUpDisplayHittersAndPitchers() {
        for (int i = 0; i < displayPlayers.size(); i++) {
            if (displayPlayers.get(i).getHitter()) {
                displayHitters.add((Hitter)displayPlayers.get(i));
            }
            else {
                displayPitchers.add((Pitcher)displayPlayers.get(i));
            }
        }
    }
    
    public ObservableList<FantasyTeam> getDisplayTeams() {
        return displayTeams;
    }
    
    public void setFTeamTableEmpty() {
        fantasyTeamsTable.setItems(emptyPlayers);
    }
    
    public void setTaxiTeamTableEmpty() {
        taxiTeamsTable.setItems(emptyPlayers);
    }
    
    public TextField getDraftNameTextField() {
        return draftNameTextField;
    }
    
    public void initGUI(String windowTitle) throws IOException {
        // INIT THE DIALOGS
        initDialogs();
        
        // INIT THE TOOLBAR
        initFileToolbar();
        
        // INIT BOTTOM TOOLBAR
        initBottomToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }
    
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            wbdkPane.setCenter(workspaceScrollPane);
            workspaceActivated = true;
        }
    }
    
    
    
    @Override
    public void reloadDraft(Draft draftToReload) {
        // FIRST ACTIVATE THE WORKSPACE IF NECESSARY
        if (!workspaceActivated) {
            activateWorkspace();
        }
    }
    
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveDraftButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadDraftButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }
    
    public void updateDraftInfo() {
        screensPane.getChildren().clear();
        screensPane.getStyleClass().add(CLASS_BORDERED_PANE_TEAL);
        screensPane.getStyleClass().remove(0);
        
        // HERE'S THE LABEL
        headingLabel = initChildLabel(screensPane, WBDK_PropertyType.DRAFT_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        screensPane.getChildren().add(draftPane);
    }
    
    public void updatePlayerInfo() {
        screensPane.getChildren().clear();
        screensPane.getStyleClass().add(CLASS_BORDERED_PANE_PURPLE);
        screensPane.getStyleClass().remove(0);
        
        // HERE'S THE LABEL
        headingLabel = initChildLabel(screensPane, WBDK_PropertyType.PLAYER_HEADING_LABEL, CLASS_HEADING_LABEL);

        // AND NOW ADD THE PANE OF THE SCREEN
        screensPane.getChildren().add(playersPane);
    }
    
    public void updateTeamInfo() {
        screensPane.getChildren().clear();
        screensPane.getStyleClass().add(CLASS_BORDERED_PANE_GREEN);
        screensPane.getStyleClass().remove(0);
        
        // HERE'S THE LABEL
        headingLabel = initChildLabel(screensPane, WBDK_PropertyType.FTEAM_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        // AND NOW ADD THE PANE OF THE SCREEN
        screensPane.getChildren().add(fantasyTeamPane);
    }
    
    public void updateMLBInfo() {
        screensPane.getChildren().clear();
        screensPane.getStyleClass().add(CLASS_BORDERED_PANE_BLUE);
        screensPane.getStyleClass().remove(0);
        
        // HERE'S THE LABEL
        headingLabel = initChildLabel(screensPane, WBDK_PropertyType.MLB_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        screensPane.getChildren().add(mlbTeamPane);
    }
    
    public void updateStandingInfo() {
        screensPane.getChildren().clear();
        screensPane.getStyleClass().add(CLASS_BORDERED_PANE_RED);
        screensPane.getStyleClass().remove(0);
        
        // HERE'S THE LABEL
        headingLabel = initChildLabel(screensPane, WBDK_PropertyType.FSTANDING_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        screensPane.getChildren().add(fantasyStandingsPane);
    }
    
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }
    
    private void initBottomToolbar() {
        bottomToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        fantasyTeamButton = initChildButton(bottomToolbarPane, WBDK_PropertyType.FTEAM_ICON, WBDK_PropertyType.FTEAM_TOOLTIP, false);
        playerButton = initChildButton(bottomToolbarPane, WBDK_PropertyType.PLAYER_ICON, WBDK_PropertyType.PLAYER_TOOLTIP, false);
        draftButton = initChildButton(bottomToolbarPane, WBDK_PropertyType.DRAFT_ICON, WBDK_PropertyType.DRAFT_TOOLTIP, false);
        mlbButton = initChildButton(bottomToolbarPane, WBDK_PropertyType.MLB_ICON, WBDK_PropertyType.MLB_TOOLTIP, false);
        fantasyStandingButton = initChildButton(bottomToolbarPane, WBDK_PropertyType.FSTANDING_ICON, WBDK_PropertyType.FSTANDING_TOOLTIP, false);
        
    }
    
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
        newDraftButton = initChildButton(fileToolbarPane, WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = initChildButton(fileToolbarPane, WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = initChildButton(fileToolbarPane, WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WBDK_PropertyType.EXIT_ICON, WBDK_PropertyType.EXIT_TOOLTIP, false);

    }
    
    private void initWorkspace() throws IOException {

        initScreens();
        initPlayerPage();
        initFantasyTeamPage();
        initDraftPage();
        initMLBPage();
        initStandingsPage();
        updateTeamInfo();

        workspacePane = new BorderPane();
        workspacePane.setCenter(screensPane);
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE_BACK);
        
        // AND NOW PUT IT IN THE WORKSPACE
        workspaceScrollPane = new ScrollPane();
        workspaceScrollPane.setContent(workspacePane);
        workspaceScrollPane.setFitToWidth(true);
        workspaceScrollPane.setFitToHeight(true);

        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }
    
     private void initScreens() {

        screensPane = new VBox();
        screensPane.getStyleClass().add(CLASS_BORDERED_PANE_GREEN);

    }
    
    private void initPlayerPage() throws IOException {

        
        // PLAYER INFO PANE HOLDS HEADING, ADD AND REMOVE BUTTONS, AND THE SEARCH BAR
        playerInfoPane = new HBox();
        playersControlPane = new BorderPane();
        playersControlButtonsPane = new HBox();
        playersSearchPane = new HBox();
        playersRadioPane = new HBox();
        playerBox = new HBox();
        
        // THE BUTTONS TO BE ADDED TO THE PAGE
        addPlayerButton = initChildButton(playersControlButtonsPane, WBDK_PropertyType.ADD_ICON, WBDK_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = initChildButton(playersControlButtonsPane, WBDK_PropertyType.MINUS_ICON, WBDK_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        
        // THE SEARCH BAR TO BE ADDED
        searchPlayerLabel = initChildLabel(playersSearchPane, WBDK_PropertyType.PLAYER_SEARCH, CLASS_SUBHEADING_LABEL);
        searchPlayerTextField = new TextField();
        searchPlayerTextField.setPrefColumnCount(60);
        playersSearchPane.getChildren().add(searchPlayerTextField);
        playersSearchPane.setPadding(new Insets(0,10,0, 50));
        
        // CREATES RADIO BUTTONS
        radioToggle = new ToggleGroup();
        allButton = new RadioButton(RADIO_ALL);
        cButton = new RadioButton(RADIO_C);
        oneB_Button = new RadioButton(RADIO_1B);
        ciButton = new RadioButton(RADIO_CI);
        threeB_Button = new RadioButton(RADIO_3B);
        twoB_Button = new RadioButton(RADIO_2B);
        miButton = new RadioButton(RADIO_MI);
        ssButton = new RadioButton(RADIO_SS);
        ofButton = new RadioButton(RADIO_OF);
        uButton = new RadioButton(RADIO_U);
        pButton = new RadioButton(RADIO_P);
        
        allButton.setToggleGroup(radioToggle);
        cButton.setToggleGroup(radioToggle);
        oneB_Button.setToggleGroup(radioToggle);
        ciButton.setToggleGroup(radioToggle);
        threeB_Button.setToggleGroup(radioToggle);
        twoB_Button.setToggleGroup(radioToggle);
        miButton.setToggleGroup(radioToggle);
        ssButton.setToggleGroup(radioToggle);
        ofButton.setToggleGroup(radioToggle);
        uButton.setToggleGroup(radioToggle);
        pButton.setToggleGroup(radioToggle);
        
        playersRadioPane.getChildren().add(allButton);
        playersRadioPane.getChildren().add(cButton);
        playersRadioPane.getChildren().add(oneB_Button);
        playersRadioPane.getChildren().add(ciButton);
        playersRadioPane.getChildren().add(threeB_Button);
        playersRadioPane.getChildren().add(twoB_Button);
        playersRadioPane.getChildren().add(miButton);
        playersRadioPane.getChildren().add(ssButton);
        playersRadioPane.getChildren().add(ofButton);
        playersRadioPane.getChildren().add(uButton);
        playersRadioPane.getChildren().add(pButton);
        
        allButton.setPadding(new Insets(5,5,5,5));
        cButton.setPadding(new Insets(5,5,5,5));
        oneB_Button.setPadding(new Insets(5,5,5,5));
        ciButton.setPadding(new Insets(5,5,5,5));
        threeB_Button.setPadding(new Insets(5,5,5,5));
        twoB_Button.setPadding(new Insets(5,5,5,5));
        miButton.setPadding(new Insets(5,5,5,5));
        ssButton.setPadding(new Insets(5,5,5,5));
        ofButton.setPadding(new Insets(5,5,5,5));
        uButton.setPadding(new Insets(5,5,5,5));
        pButton.setPadding(new Insets(5,5,5,5));
        
        allButton.setSelected(true);
        playersRadioPane.setPadding(new Insets(30,30,30,30));
        playersRadioPane.getStyleClass().add(CLASS_BORDERED_PANE_PURPLE);
        
        playersControlPane.setCenter(playersSearchPane);
        playersControlPane.setLeft(playersControlButtonsPane);
        playerInfoPane.getChildren().add(playersControlPane);
        playerInfoPane.getStyleClass().add(CLASS_BORDERED_PANE_PURPLE);
        
        //CREATES THE TABLE
        playerTable = new TableView<Player>();
        firstColumn = new TableColumn(COL_FIRST);
        lastColumn = new TableColumn(COL_LAST);
        proTeamColumn = new TableColumn(COL_PRO_TEAM);
        positionsColumn = new TableColumn(COL_POSITIONS);
        yearOfBirthColumn = new TableColumn(COL_YEAR_OF_BIRTH);
        r_w_Column = new TableColumn(COL_R_W);
        hr_sv_Column = new TableColumn(COL_HR_SV);
        rbi_k_Column = new TableColumn(COL_RBI_K);
        sb_era_Column = new TableColumn(COL_SB_ERA);
        ba_whip_Column = new TableColumn(COL_BA_WHIP);
        estimatedValueColumn = new TableColumn(COL_ESTIMATED_VALUE);
        notesColumn = new TableColumn(COL_NOTES);
        //randomColumn = new TableColumn(EMPTY_TEXT);
        
        //LINKS DATA TO THE TABLE
        firstColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("firstName"));
        lastColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lastName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        positionsColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("pos"));
        yearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("yearOfBirth"));
        r_w_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("r_w"));
        hr_sv_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("hr_sv"));
        rbi_k_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("rbi_k"));
        sb_era_Column.setCellValueFactory(new PropertyValueFactory<Player, Double>("sb_era"));
        ba_whip_Column.setCellValueFactory(new PropertyValueFactory<Player, Double>("ba_whip"));
        estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("estimatedValue"));
        notesColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("notes"));
//        randomColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("nationOfBirth"));
        
        playerTable.getColumns().add(firstColumn);
        playerTable.getColumns().add(lastColumn);
        playerTable.getColumns().add(proTeamColumn);
        playerTable.getColumns().add(positionsColumn);
        playerTable.getColumns().add(yearOfBirthColumn);
        playerTable.getColumns().add(r_w_Column);
        playerTable.getColumns().add(hr_sv_Column);
        playerTable.getColumns().add(rbi_k_Column);
        playerTable.getColumns().add(sb_era_Column);
        playerTable.getColumns().add(ba_whip_Column);
        playerTable.getColumns().add(estimatedValueColumn);
        playerTable.getColumns().add(notesColumn);
        //playerTable.getColumns().add(randomColumn);
        
        notesColumn.setEditable(true);
        notesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        notesColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<Player, String>>() {
                @Override
                public void handle(CellEditEvent<Player, String> t) {
                    ((Player) t.getTableView().getItems().get(t.getTablePosition().getRow())).setNotes(t.getNewValue());
                }
             }
        );
        playerTable.setEditable(true);
        
        playerBox.getChildren().add(playerTable);
        playerBox.getStyleClass().add(CLASS_BORDERED_PANE_PURPLE);
        
        //POPULATE THE TABLE
        for (int i = 0; i < dataManager.getPlayerHolder().getPlayers().size(); i++) {
            displayPlayers.add(dataManager.getPlayerHolder().getPlayers().get(i));
        }
        playerTable.setItems(displayPlayers);
        for (int i = 0; i < dataManager.getPlayerHolder().getPlayers().size(); i++) {
            displayMLB.add(dataManager.getPlayerHolder().getPlayers().get(i));
        }
        
        // THIS IS WHAT HOLDS EVERYTHING IN THE PLAYER PAGE
        playersPane = new VBox();
        playersPane.getChildren().add(playerInfoPane);
        playersPane.getChildren().add(playersRadioPane);
        playersPane.getChildren().add(playerBox);
        playersPane.getStyleClass().add(CLASS_BORDERED_PANE_GREY);
        
    }
    
    private void initFantasyTeamPage() throws IOException{
        fantasyTeamPane = new VBox();
        draftBarPane = new HBox();
        fantasyTeamControlPane = new VBox();
        fantasyTeamButtonPane = new HBox();
        fantasyTeamTablePane = new VBox();
        taxiTeamTablePane = new VBox();
        selectFTeamComboPane = new HBox();
        
        //CREATES DRAFT NAME TEXT FIELD
        draftNameLabel = initChildLabel(draftBarPane, WBDK_PropertyType.NEW_DRAFT_NAME, CLASS_SUBHEADING_LABEL);
        draftNameTextField = new TextField();
        draftNameTextField.setPrefColumnCount(60);
        draftBarPane.getChildren().add(draftNameTextField);
        draftBarPane.setPadding(new Insets(0,10,0, 0));
        fantasyTeamControlPane.getChildren().add(draftBarPane);
        
        //ADD, REMOVE, EDIT TEAM BUTTONS
        addTeamButton = initChildButton(fantasyTeamButtonPane, WBDK_PropertyType.ADD_ICON, WBDK_PropertyType.ADD_TEAM_TOOLTIP, false);
        removeTeamButton = initChildButton(fantasyTeamButtonPane, WBDK_PropertyType.MINUS_ICON, WBDK_PropertyType.REMOVE_TEAM_TOOLTIP, true);
        editTeamButton = initChildButton(fantasyTeamButtonPane, WBDK_PropertyType.EDIT_ICON, WBDK_PropertyType.REMOVE_TEAM_TOOLTIP, true);
        selectFTeamLabel = initChildLabel(selectFTeamComboPane, WBDK_PropertyType.SELECT_F_TEAM, CLASS_SUBHEADING_LABEL);
        
        //CREATES THE COMBO BOX
        fantasyTeamComboBox = new ComboBox();
        for (int i = 0; i < dataManager.getTeamHolder().getTeams().size(); i++) {
            displayTeams.add(dataManager.getTeamHolder().getTeams().get(i));
        }
       
        
        //ADDS THE COMBO BOX AND BUTTONS TO THE PANE
        selectFTeamComboPane.getChildren().add(fantasyTeamComboBox);
        selectFTeamComboPane.setPadding(new Insets(0,0,0,20));
        fantasyTeamButtonPane.getChildren().add(selectFTeamComboPane);
        fantasyTeamControlPane.getChildren().add(fantasyTeamButtonPane);
        fantasyTeamControlPane.getStyleClass().add(CLASS_BORDERED_PANE_GREEN);
        
        //CREATES THE FANTASY TEAM TABLE
        fantasyTeamTableLabel = initChildLabel(fantasyTeamTablePane, WBDK_PropertyType.STARTING_LINEUP, CLASS_SUBHEADING_LABEL);
        fantasyTeamsTable = new TableView<Player>();
        
        ftPositionColumn = new TableColumn(COL_POSITION);
        ftFirstColumn = new TableColumn(COL_FIRST);
        ftLastColumn = new TableColumn(COL_LAST);
        ftProColumn = new TableColumn(COL_PRO_TEAM);
        ftPositionsColumn = new TableColumn(COL_POSITIONS);
        ft_R_W_Column = new TableColumn(COL_R_W);
        ft_HR_SV_Column = new TableColumn(COL_HR_SV);
        ft_RBI_K_Column = new TableColumn(COL_RBI_K);
        ft_SB_ERA_Column = new TableColumn(COL_SB_ERA);
        ft_BA_WHIP_Column = new TableColumn(COL_BA_WHIP);
        ft_Value_Column = new TableColumn(COL_ESTIMATED_VALUE);
        ft_Contract_Column = new TableColumn(COL_CONTRACT);
        ft_Salary_Column = new TableColumn(COL_SALARY);
        
        //LINKS DATA TO THE TABLE
        ftPositionColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("playingPosition"));
        ftFirstColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("firstName"));
        ftLastColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lastName"));
        ftProColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        ftPositionsColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("pos"));
        ft_R_W_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("r_w"));
        ft_HR_SV_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("hr_sv"));
        ft_RBI_K_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("rbi_k"));
        ft_SB_ERA_Column.setCellValueFactory(new PropertyValueFactory<Player, Double>("sb_era"));
        ft_BA_WHIP_Column.setCellValueFactory(new PropertyValueFactory<Player, Double>("ba_whip"));
        ft_Value_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("estimatedValue"));
        ft_Contract_Column.setCellValueFactory(new PropertyValueFactory<Player, String>("contract"));
        ft_Salary_Column.setCellValueFactory(new PropertyValueFactory<Player, String>("salary"));
        
        fantasyTeamsTable.getColumns().add(ftPositionColumn);
        fantasyTeamsTable.getColumns().add(ftFirstColumn);
        fantasyTeamsTable.getColumns().add(ftLastColumn);
        fantasyTeamsTable.getColumns().add(ftProColumn);
        fantasyTeamsTable.getColumns().add(ftPositionsColumn);
        fantasyTeamsTable.getColumns().add(ft_R_W_Column);
        fantasyTeamsTable.getColumns().add(ft_HR_SV_Column);
        fantasyTeamsTable.getColumns().add(ft_RBI_K_Column);
        fantasyTeamsTable.getColumns().add(ft_SB_ERA_Column);
        fantasyTeamsTable.getColumns().add(ft_BA_WHIP_Column);
        fantasyTeamsTable.getColumns().add(ft_Value_Column);
        fantasyTeamsTable.getColumns().add(ft_Contract_Column);
        fantasyTeamsTable.getColumns().add(ft_Salary_Column);
        
        fantasyTeamTablePane.getChildren().add(fantasyTeamsTable);
        fantasyTeamTablePane.getStyleClass().add(CLASS_BORDERED_PANE_GREEN);
        
        //CREATES THE TAXI TABLE
        taxiTeamTableLabel = initChildLabel(taxiTeamTablePane, WBDK_PropertyType.TAXI_LINEUP, CLASS_SUBHEADING_LABEL);
        taxiTeamsTable = new TableView<Player>();
        
        taxiPositionColumn = new TableColumn(COL_POSITION);
        taxiFirstColumn = new TableColumn(COL_FIRST);
        taxiLastColumn = new TableColumn(COL_LAST);
        taxiProColumn = new TableColumn(COL_PRO_TEAM);
        taxiPositionsColumn = new TableColumn(COL_POSITIONS);
        taxi_R_W_Column = new TableColumn(COL_R_W);
        taxi_HR_SV_Column = new TableColumn(COL_HR_SV);
        taxi_RBI_K_Column = new TableColumn(COL_RBI_K);
        taxi_SB_ERA_Column = new TableColumn(COL_SB_ERA);
        taxi_BA_WHIP_Column = new TableColumn(COL_BA_WHIP);
        taxi_Value_Column = new TableColumn(COL_ESTIMATED_VALUE);
        taxi_Contract_Column = new TableColumn(COL_CONTRACT);
        taxi_Salary_Column = new TableColumn(COL_SALARY);
        
        taxiPositionColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("playingPosition"));
        taxiFirstColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("firstName"));
        taxiLastColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lastName"));
        taxiProColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        taxiPositionsColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("pos"));
        taxi_R_W_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("r_w"));
        taxi_HR_SV_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("hr_sv"));
        taxi_RBI_K_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("rbi_k"));
        taxi_SB_ERA_Column.setCellValueFactory(new PropertyValueFactory<Player, Double>("sb_era"));
        taxi_BA_WHIP_Column.setCellValueFactory(new PropertyValueFactory<Player, Double>("ba_whip"));
        taxi_Value_Column.setCellValueFactory(new PropertyValueFactory<Player, Integer>("estimatedValue"));
        taxi_Contract_Column.setCellValueFactory(new PropertyValueFactory<Player, String>("contract"));
        taxi_Salary_Column.setCellValueFactory(new PropertyValueFactory<Player, String>("salary"));
        
        taxiTeamsTable.getColumns().add(taxiPositionColumn);
        taxiTeamsTable.getColumns().add(taxiFirstColumn);
        taxiTeamsTable.getColumns().add(taxiLastColumn);
        taxiTeamsTable.getColumns().add(taxiProColumn);
        taxiTeamsTable.getColumns().add(taxiPositionsColumn);
        taxiTeamsTable.getColumns().add(taxi_R_W_Column);
        taxiTeamsTable.getColumns().add(taxi_HR_SV_Column);
        taxiTeamsTable.getColumns().add(taxi_RBI_K_Column);
        taxiTeamsTable.getColumns().add(taxi_SB_ERA_Column);
        taxiTeamsTable.getColumns().add(taxi_BA_WHIP_Column);
        taxiTeamsTable.getColumns().add(taxi_Value_Column);
        taxiTeamsTable.getColumns().add(taxi_Contract_Column);
        taxiTeamsTable.getColumns().add(taxi_Salary_Column);
        
        taxiTeamTablePane.getChildren().add(taxiTeamsTable);
        taxiTeamTablePane.getStyleClass().add(CLASS_BORDERED_PANE_GREEN);
        

        //THIS IS WHAT HOLDS EVERYTHING IN THE FANTASY TEAM PAGE
        fantasyTeamPane.getChildren().add(fantasyTeamControlPane);
        fantasyTeamPane.getChildren().add(fantasyTeamTablePane);
        fantasyTeamPane.getChildren().add(taxiTeamTablePane);
        fantasyTeamPane.getStyleClass().add(CLASS_BORDERED_PANE_GREY);
    }
    
    private void initDraftPage() {
        
        draftPane = new VBox();
        draftTablePane = new VBox();
        draftControlsPane = new HBox();
        draftTable = new TableView<Player>();
        
        draftPick = new TableColumn(COL_PICK);
        draftFirstName = new TableColumn(COL_FIRST);
        draftLastName = new TableColumn(COL_LAST);
        draftFTeamName = new TableColumn(COL_FTEAM);
        draftContract = new TableColumn(COL_CONTRACT);
        draftSalary = new TableColumn(COL_SALARY);
        
        starDraft = initChildButton(draftControlsPane, WBDK_PropertyType.STAR_ICON, WBDK_PropertyType.STAR_TOOLTIP, false);
        playDraft = initChildButton(draftControlsPane, WBDK_PropertyType.PLAY_ICON, WBDK_PropertyType.PLAY_TOOLTIP, false);
        pauseDraft = initChildButton(draftControlsPane, WBDK_PropertyType.PAUSE_ICON, WBDK_PropertyType.PAUSE_TOOLTIP, false);
        draftControlsPane.getStyleClass().add(CLASS_BORDERED_PANE_TEAL);
        
        draftPick.setCellValueFactory(new PropertyValueFactory<Player, String>("pickNumber"));
        draftFirstName.setCellValueFactory(new PropertyValueFactory<Player, String>("firstName"));
        draftLastName.setCellValueFactory(new PropertyValueFactory<Player, String>("lastName"));
        draftFTeamName.setCellValueFactory(new PropertyValueFactory<Player, String>("fTeam"));
        draftContract.setCellValueFactory(new PropertyValueFactory<Player, Integer>("contract"));
        draftSalary.setCellValueFactory(new PropertyValueFactory<Player, Integer>("salary"));
        
        draftTable.getColumns().add(draftPick);
        draftTable.getColumns().add(draftFirstName);
        draftTable.getColumns().add(draftLastName);
        draftTable.getColumns().add(draftFTeamName);
        draftTable.getColumns().add(draftContract);
        draftTable.getColumns().add(draftSalary);
        draftTable.setItems(draftedPlayers);
        draftTablePane.getStyleClass().add(CLASS_BORDERED_PANE_TEAL);
        
        draftTablePane.getChildren().add(draftTable);
        draftPane.getChildren().add(draftControlsPane);
        draftPane.getChildren().add(draftTablePane);
        draftPane.getStyleClass().add(CLASS_BORDERED_PANE_GREY);
    }
    
    private void initMLBPage() {
        
        // POPULATE LIST OF MLB TEAMS SO WE CAN POPULATE COMBO BOX
        mlbDisplayTeams.add(MLB_ATL);
        mlbDisplayTeams.add(MLB_AZ);
        mlbDisplayTeams.add(MLB_CHC);
        mlbDisplayTeams.add(MLB_CIN);
        mlbDisplayTeams.add(MLB_COL);
        mlbDisplayTeams.add(MLB_LAD);
        mlbDisplayTeams.add(MLB_MIA);
        mlbDisplayTeams.add(MLB_MIL);
        mlbDisplayTeams.add(MLB_NYM);
        mlbDisplayTeams.add(MLB_PHI);
        mlbDisplayTeams.add(MLB_PIT);
        mlbDisplayTeams.add(MLB_SD);
        mlbDisplayTeams.add(MLB_SF);
        mlbDisplayTeams.add(MLB_STL);
        mlbDisplayTeams.add(MLB_WAS);
        
        mlbComboBox = new ComboBox<String>();
        mlbComboBoxLabel = new Label();
        mlbControlsPane = new HBox();
        mlbComboBoxLabel = initChildLabel(mlbControlsPane, WBDK_PropertyType.SELECT_PRO_TEAM, CLASS_SUBHEADING_LABEL);
        mlbComboBox.setItems(mlbDisplayTeams);
        mlbControlsPane.getChildren().add(mlbComboBox);
        mlbControlsPane.getStyleClass().add(CLASS_BORDERED_PANE_BLUE);
        
        // SETS UP THE TABLE
        mlbTeamTablePane = new VBox();
        mlbTeamsTable = new TableView<Player>();
        
        mlbProTeamColumn = new TableColumn(COL_PRO_TEAM);
        mlbLastNameColumn = new TableColumn(COL_LAST);
        mlbFirstNameColumn = new TableColumn(COL_FIRST);
        mlbQualifiedPositionsColumn = new TableColumn(COL_POSITIONS);
        
        mlbTeamsTable.getColumns().add(mlbProTeamColumn);
        mlbTeamsTable.getColumns().add(mlbLastNameColumn);
        mlbTeamsTable.getColumns().add(mlbFirstNameColumn);
        mlbTeamsTable.getColumns().add(mlbQualifiedPositionsColumn);
        
        mlbProTeamColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("team"));
        mlbLastNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("lastName"));
        mlbFirstNameColumn.setCellValueFactory(new PropertyValueFactory<Player, String>("firstName"));
        mlbQualifiedPositionsColumn.setCellValueFactory(new PropertyValueFactory<Player, Integer>("pos"));
        
        mlbTeamTablePane.getChildren().add(mlbTeamsTable);
        mlbTeamTablePane.getStyleClass().add(CLASS_BORDERED_PANE_BLUE);
        
        mlbTeamPane = new VBox();
        mlbTeamPane.getChildren().add(mlbControlsPane);
        mlbTeamPane.getChildren().add(mlbTeamTablePane);
        mlbTeamPane.getStyleClass().add(CLASS_BORDERED_PANE_GREY);
    }
    
    private void initStandingsPage() {
        
        fantasyStandingsPane = new VBox();
        standingsTablePane = new VBox();
        standingsTable = new TableView<FantasyTeam>();
        
        standingsTeamColumn = new TableColumn(COL_FTEAM);
        standingsPlayerNeededColumn = new TableColumn(COL_PLAYERS_NEEDED);
        standingsMoneyLeftColumn = new TableColumn(COL_BUDGET_LEFT);
        standingsMoneyPerPlayerColumn = new TableColumn(COL_PRICE_PER_PLAYER);
        standingsRColumn = new TableColumn(COL_R);
        standingsHRColumn = new TableColumn(COL_HR);
        standingsRBIColumn = new TableColumn(COL_RBI);
        standingsSBColumn = new TableColumn(COL_SB);
        standingsBAColumn = new TableColumn(COL_BA);
        standingsWColumn = new TableColumn(COL_W);
        standingsSVColumn = new TableColumn(COL_SV);
        standingsKColumn = new TableColumn(COL_K);
        standingsERAColumn = new TableColumn(COL_ERA);
        standingsWHIPColumn = new TableColumn(COL_WHIP);
        standingsTotalPointsColumn = new TableColumn(COL_TOTAL_POINTS);
        
        standingsTable.getColumns().add(standingsTeamColumn);
        standingsTable.getColumns().add(standingsPlayerNeededColumn);
        standingsTable.getColumns().add(standingsMoneyLeftColumn);
        standingsTable.getColumns().add(standingsMoneyPerPlayerColumn);
        standingsTable.getColumns().add(standingsRColumn);
        standingsTable.getColumns().add(standingsHRColumn);
        standingsTable.getColumns().add(standingsRBIColumn);
        standingsTable.getColumns().add(standingsSBColumn);
        standingsTable.getColumns().add(standingsBAColumn);
        standingsTable.getColumns().add(standingsWColumn);
        standingsTable.getColumns().add(standingsSVColumn);
        standingsTable.getColumns().add(standingsKColumn);
        standingsTable.getColumns().add(standingsERAColumn);
        standingsTable.getColumns().add(standingsWHIPColumn);
        standingsTable.getColumns().add(standingsTotalPointsColumn);
        
        standingsTeamColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, String>("teamName"));
        standingsPlayerNeededColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("playersNeeded"));
        standingsMoneyLeftColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("totalBudget"));
        standingsMoneyPerPlayerColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("pricePerPlayer"));
        standingsRColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("standingsR"));
        standingsHRColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("standingsHR"));
        standingsRBIColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("standingsRBI"));
        standingsSBColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("standingsSB"));
        standingsBAColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("standingsBA"));
        standingsWColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("standingsW"));
        standingsSVColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("standingsSV"));
        standingsKColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("standingsK"));
        standingsERAColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("standingsERA"));
        standingsWHIPColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Double>("standingsWHIP"));
        standingsTotalPointsColumn.setCellValueFactory(new PropertyValueFactory<FantasyTeam, Integer>("totalPoints"));
        
        standingsTable.setItems(displayTeams);
        
        standingsTablePane.getChildren().add(standingsTable);
        standingsTablePane.getStyleClass().add(CLASS_BORDERED_PANE_RED);
        fantasyStandingsPane.getChildren().add(standingsTablePane);
        fantasyStandingsPane.getStyleClass().add(CLASS_BORDERED_PANE_GREY);
    }
    
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        wbdkPane = new BorderPane();
        wbdkPane.setTop(fileToolbarPane);
        primaryScene = new Scene(wbdkPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);        
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    
    private Button initChildButton(Pane toolbar, WBDK_PropertyType icon, WBDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    private Label initLabel(WBDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    // INIT A LABEL AND PLACE IT IN A GridPane INIT ITS PROPER PLACE
    private Label initGridLabel(GridPane container, WBDK_PropertyType labelProperty, String styleClass, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }

    private Label initChildLabel(Pane container, WBDK_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
     
    private void initEventHandlers() throws IOException {
        
        // TOP TOOLBAR
        fileController = new FileController(messageDialog, yesNoCancelDialog, fileManager);
        newDraftButton.setOnAction(e -> {
            workspacePane.setBottom(bottomToolbarPane);
            fileController.handleNewDraftRequest(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
        });
        loadDraftButton.setOnAction(e -> {
            workspacePane.setBottom(bottomToolbarPane);
            fileController.handleLoadDraftRequest(this);
            if (!fantasyTeamComboBox.getItems().isEmpty()) {
                removeTeamButton.setDisable(false);
                editTeamButton.setDisable(false);
            }
        });
        saveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });
        
        // BOTTOM TOOLBAR
        fantasyTeamButton.setOnAction(e -> {
            updateTeamInfo();
        });
        playerButton.setOnAction(e -> {
            updatePlayerInfo();
        });
        draftButton.setOnAction(e -> {
            updateDraftInfo();
        });
        mlbButton.setOnAction(e -> {
            updateMLBInfo();
        });
        fantasyStandingButton.setOnAction(e -> {
            updateStandingInfo();
        });
        
        // EDITING PLAYERS TABLE
        playerController = new PlayerController(primaryStage, dataManager.getPlayerHolder(), messageDialog, yesNoCancelDialog, dataManager.getTeamHolder());
        playerTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player player = playerTable.getSelectionModel().getSelectedItem();
                playerController.handleEditPlayerRequest(this, player, dataManager.getTeamHolder(), primaryStage, messageDialog);
                fileController.markAsEdited(this);
                reloadFTeamPageTable();
                if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                    saveDraftButton.setDisable(true);
                }
                updatePickNumber();
            }
        });
        removePlayerButton.setOnAction(e -> {
            Player playerToRemove = playerTable.getSelectionModel().getSelectedItem();
            dataManager.getPlayerHolder().removePlayer(playerToRemove);
            if (playerToRemove.getHitter()) {
                dataManager.getPlayerHolder().getHitters().remove(playerToRemove);
                displayHitters.remove(playerToRemove);
            }
            else {
                dataManager.getPlayerHolder().getPitchers().remove(playerToRemove);
                displayPitchers.remove(playerToRemove);
            }
            displayPlayers.remove(playerToRemove);
            displayMLB.remove(playerToRemove);
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
        });
        addPlayerButton.setOnAction(e -> {
            Player player = playerTable.getSelectionModel().getSelectedItem();
            playerController.handleAddPlayerRequest(this, primaryStage, messageDialog);
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
        });
        
        //DRAFT NAME TEXTFIELD
        draftNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            dataManager.getDraft().setDraftName(newValue);
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
        });
        
        // RADIO BUTTON SELECTION
        allButton.setOnAction(e -> {
            sortTableAll();
        });
        cButton.setOnAction(e -> {
            newTableHitterSort(RADIO_C);
        });
        oneB_Button.setOnAction(e -> {
            newTableHitterSort(RADIO_1B);
        });
        ciButton.setOnAction(e -> {
            newTableHitterSort(RADIO_CI);
        });
        threeB_Button.setOnAction(e -> {
            newTableHitterSort(RADIO_3B);
        });
        twoB_Button.setOnAction(e -> {
            newTableHitterSort(RADIO_2B);
        });
        miButton.setOnAction(e -> {
            newTableHitterSort(RADIO_MI);
        });
        ssButton.setOnAction(e -> {
            newTableHitterSort(RADIO_SS);
        });
        ofButton.setOnAction(e -> {
            newTableHitterSort(RADIO_OF);
        });
        uButton.setOnAction(e -> {
            newTableHitterSort(RADIO_U);
        });
        pButton.setOnAction(e -> {
            sortTablePitcher();
        });
        
        //SEARCH BAR EVENT HANDLER
        searchPlayerTextField.textProperty().addListener((observable, oldValue, newValue)-> {
           if (allButton.isSelected()) {
                sortTableAll();
            }
            else if (cButton.isSelected()) {
                newTableHitterSort(RADIO_C);
            }
            else if (oneB_Button.isSelected()) {
                newTableHitterSort(RADIO_1B);
            }
            else if (ciButton.isSelected()) {
                newTableHitterSort(RADIO_CI);
            }
            else if (threeB_Button.isSelected()) {
                newTableHitterSort(RADIO_3B);
            }
            else if (twoB_Button.isSelected()) {
                newTableHitterSort(RADIO_2B);
            }
            else if (miButton.isSelected()) {
                newTableHitterSort(RADIO_MI);
            }
            else if (ssButton.isSelected()) {
                newTableHitterSort(RADIO_SS);
            }
            else if (ofButton.isSelected()) {
                newTableHitterSort(RADIO_OF);
            }
            else if (uButton.isSelected()) {
                newTableHitterSort(RADIO_U);
            }
            else if (pButton.isSelected()) {
                sortTablePitcher();
            }
        });
        
        
        //FANTASY TEAM CONTROLS
        teamController = new FantasyTeamController(primaryStage, dataManager.getTeamHolder(), messageDialog, yesNoCancelDialog);
        addTeamButton.setOnAction(e -> {
            teamController.handleAddTeamRequest(this);
            reloadTeamComboBox();
            if (!fantasyTeamComboBox.getItems().isEmpty()) {
                removeTeamButton.setDisable(false);
                editTeamButton.setDisable(false);
            }
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
        });
        editTeamButton.setOnAction(e -> {
            teamController.handleEditTeamRequest(this, getTeamFromName());
            reloadTeamComboBox();
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
        });
        removeTeamButton.setOnAction(e -> {
            teamController.handleRemoveTeamRequest(this, getTeamFromName());
            reloadTeamComboBox();
            if (fantasyTeamComboBox.getItems().isEmpty()) {
            fantasyTeamsTable.setItems(emptyPlayers);
            taxiTeamsTable.setItems(emptyPlayers);
            removeTeamButton.setDisable(true);
            editTeamButton.setDisable(true);  
            }
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
//            reloadFTeamPageTable();
            updatePickNumber();
        });
        fantasyTeamsTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player player = fantasyTeamsTable.getSelectionModel().getSelectedItem();
                playerController.handleEditPlayerRequest2(this, player, dataManager.getTeamHolder(), primaryStage, messageDialog);
//                fileController.markAsEdited(this);
                reloadFTeamPageTable();
            }
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
            updatePickNumber();
        });
        taxiTeamsTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Player player = taxiTeamsTable.getSelectionModel().getSelectedItem();
                playerController.handleEditPlayerRequest3(this, player, dataManager.getTeamHolder(), primaryStage, messageDialog);
//                fileController.markAsEdited(this);
                reloadFTeamPageTable();
            }
            fileController.markAsEdited(this);
            if (draftNameTextField.getText().compareToIgnoreCase("") == 0) {
                saveDraftButton.setDisable(true);
            }
            updatePickNumber();
        });
        
        fantasyTeamComboBox.setOnAction(e -> {
            reloadFTeamPageTable();
        });
        
        mlbComboBox.setOnAction(e -> {
            sortMLB();
        });
        
        // DRAFT PAGE CONTROLS
        starDraft.setOnAction(e -> {
            playerController.draftPlayer(this, dataManager.getTeamHolder(), dataManager.getPlayerHolder());
        });
        playDraft.setOnAction(e -> {
            playerController.automatedDraft(this, dataManager.getTeamHolder(), dataManager.getPlayerHolder());
        });
        pauseDraft.setOnAction(e -> {
            playerController.isPauseButtonPressed();
        });
    }
     
    private void sortTableAll() {
        displayPlayers.removeAll(displayPlayers);
        for (int i = 0; i < dataManager.getPlayerHolder().getPlayers().size(); i++) {
            if (dataManager.getPlayerHolder().getPlayers().get(i).getFirstName().toLowerCase().contains(searchPlayerTextField.getText().toLowerCase()) ||
                dataManager.getPlayerHolder().getPlayers().get(i).getLastName().toLowerCase().contains(searchPlayerTextField.getText().toLowerCase())) {
                
                if (dataManager.getPlayerHolder().getPlayers().get(i).getFTeam().compareToIgnoreCase("") == 0) {
                    displayPlayers.add(dataManager.getPlayerHolder().getPlayers().get(i));
                }
                Player player = dataManager.getPlayerHolder().getPlayers().get(i);
            }
        }
        
        r_w_Column.setText(COL_R_W);
        hr_sv_Column.setText(COL_HR_SV);
        rbi_k_Column.setText(COL_RBI_K);
        sb_era_Column.setText(COL_SB_ERA);
        ba_whip_Column.setText(COL_BA_WHIP);
    }
    
    private void sortTablePitcher() {
        displayPlayers.removeAll(displayPlayers);
        for (int i = 0; i < dataManager.getPlayerHolder().getPitchers().size(); i++) {
            if (dataManager.getPlayerHolder().getPitchers().get(i).getFirstName().toLowerCase().contains(searchPlayerTextField.getText().toLowerCase()) ||
                dataManager.getPlayerHolder().getPitchers().get(i).getLastName().toLowerCase().contains(searchPlayerTextField.getText().toLowerCase())) {
                
                if (dataManager.getPlayerHolder().getPitchers().get(i).getFTeam().compareToIgnoreCase("") == 0) {
                    displayPlayers.add(dataManager.getPlayerHolder().getPitchers().get(i));
                }
            }
        }
        r_w_Column.setText(COL_W);
        hr_sv_Column.setText(COL_SV);
        rbi_k_Column.setText(COL_K);
        sb_era_Column.setText(COL_ERA);
        ba_whip_Column.setText(COL_WHIP);
    }
    
    
    private void newTableHitterSort(String position) {
        displayPlayers.removeAll(displayPlayers);
        for (int i = 0; i < dataManager.getPlayerHolder().getHitters().size(); i++) {
            String[] dataList = (dataManager.getPlayerHolder().getHitters().get(i)).getQPList();
            
            for (int j = 0; j < dataList.length; j++) {
                if (dataList[j].compareTo(position) == 0) {
                    if (dataManager.getPlayerHolder().getHitters().get(i).getFirstName().toLowerCase().contains(searchPlayerTextField.getText().toLowerCase()) ||
                        dataManager.getPlayerHolder().getHitters().get(i).getLastName().toLowerCase().contains(searchPlayerTextField.getText().toLowerCase())) {
                    
                        if (dataManager.getPlayerHolder().getHitters().get(i).getFTeam().compareToIgnoreCase("") == 0) {
                            displayPlayers.add(dataManager.getPlayerHolder().getHitters().get(i));
                        }
                    }
                }
            }
        }
        r_w_Column.setText(COL_R);
        hr_sv_Column.setText(COL_HR);
        rbi_k_Column.setText(COL_RBI);
        sb_era_Column.setText(COL_SB);
        ba_whip_Column.setText(COL_BA);
    }
    
    private void sortMLB() {
        
        String selectedMLB = new String();
        selectedMLB = mlbComboBox.getSelectionModel().getSelectedItem();
        displayMLB.removeAll(displayMLB);
        for (int i = 0; i < dataManager.getPlayerHolder().getPlayers().size(); i++) {
            if (dataManager.getPlayerHolder().getPlayers().get(i).getTeam().compareToIgnoreCase(selectedMLB) == 0) {
                displayMLB.add(dataManager.getPlayerHolder().getPlayers().get(i));
            }
        }
        
        mlbTeamsTable.setItems(displayMLB);
        
        mlbTeamsTable.getSortOrder().add(0, mlbLastNameColumn);
        mlbTeamsTable.getSortOrder().add(1, mlbFirstNameColumn);
    }
    
    public void reloadTeamComboBox() {
        displayTeams.removeAll(displayTeams);
        for (int i = 0; i < dataManager.getTeamHolder().getTeams().size(); i++) {
            displayTeams.add(dataManager.getTeamHolder().getTeams().get(i));
        }
        ObservableList<String> choice = FXCollections.observableArrayList();
            for (FantasyTeam s : displayTeams) {
                choice.add(s.getTeamName());
            }
            fantasyTeamComboBox.setItems(choice);
            fantasyTeamComboBox.getSelectionModel().selectFirst();
    }
    
    private void reloadFTeamPageTable() {
        for (int x = 0; x < displayTeams.size(); x++) {
            if (fantasyTeamComboBox.getItems().isEmpty()) {
                fantasyTeamsTable.setItems(emptyPlayers);
                taxiTeamsTable.setItems(emptyPlayers);
            }
            else {
                if (fantasyTeamComboBox.getSelectionModel().getSelectedItem() != null) {
                   if (fantasyTeamComboBox.getSelectionModel().getSelectedItem().toString().compareToIgnoreCase(displayTeams.get(x).getTeamName()) == 0) {
                        fantasyTeamsTable.setItems(displayTeams.get(x).getListOfActivePlayers());
                        taxiTeamsTable.setItems(displayTeams.get(x).getListOfTaxiPlayers());
                    } 
                }
            }
        }
    }
    
    private FantasyTeam getTeamFromName() {
        FantasyTeam team = new FantasyTeam();
        for (int i = 0; i < displayTeams.size(); i++) {
            if (displayTeams.get(i).getTeamName().compareToIgnoreCase((String)fantasyTeamComboBox.getSelectionModel().getSelectedItem()) == 0) {
                team = displayTeams.get(i);
                    
            }
        }
        return team;
    }
    
    public void resetGUI() {
        displayPlayers.clear();
        //POPULATE THE TABLE
        for (int i = 0; i < dataManager.getPlayerHolder().getPlayers().size(); i++) {
            displayPlayers.add(dataManager.getPlayerHolder().getPlayers().get(i));
        }
        playerTable.setItems(displayPlayers);
        draftedPlayers.clear();
        searchPlayerTextField.setText("");
        reloadTeamComboBox();
        sortTableAll();
    }
    
    public void updatePickNumber() {
        for (int i = 0; i < draftedPlayers.size(); i++) {
            draftedPlayers.get(i).setPickNumber(i+1);
        }
    }
    
    public void updateDraftTable() {
        draftTable.getSortOrder().add(0, draftPick);
    }
}


