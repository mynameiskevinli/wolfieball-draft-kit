/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author Kevin
 */
public class PlayerHolder {
    
    ObservableList<Hitter> hitters;
    ObservableList<Pitcher> pitchers;
    ObservableList<Player> players;
//    ObservableList<Player> freeAgents;
    
    ObservableList<Integer> statsR;
    ObservableList<Integer> statsHR;
    ObservableList<Integer> statsRBI;
    ObservableList<Integer> statsSB;
    ObservableList<Double> statsBA;
    ObservableList<Integer> statsW;
    ObservableList<Integer> statsSV;
    ObservableList<Integer> statsK;
    ObservableList<Double> statsERA;
    ObservableList<Double> statsWHIP;
    
    ObservableList<Integer> pitchersRankings;
    ObservableList<Integer> hittersRankings;
    
//    int counter;
    
    public PlayerHolder() {
        
        hitters = FXCollections.observableArrayList();
        pitchers = FXCollections.observableArrayList();
        players = FXCollections.observableArrayList();
//        freeAgents = FXCollections.observableArrayList();
        
        statsR = FXCollections.observableArrayList();
        statsHR = FXCollections.observableArrayList();
        statsRBI = FXCollections.observableArrayList();
        statsSB = FXCollections.observableArrayList();
        statsBA = FXCollections.observableArrayList();
        statsW = FXCollections.observableArrayList();
        statsSV = FXCollections.observableArrayList();
        statsK = FXCollections.observableArrayList();
        statsERA = FXCollections.observableArrayList();
        statsWHIP = FXCollections.observableArrayList();
        
        hittersRankings = FXCollections.observableArrayList();
        pitchersRankings = FXCollections.observableArrayList();
        
//        counter = 1;
    }
    
    public ObservableList<Hitter> getHitters() {
        return hitters;
    }
    
    public void addHitter (Hitter h) {
        hitters.add(h);
        Collections.sort(hitters);
    }
    
    public void removeHitter (Hitter h) {
        hitters.remove(h);
    }
    
    public ObservableList<Pitcher> getPitchers() {
        return pitchers;
    }
    
    public void addPitcher (Pitcher p) {
        pitchers.add(p);
        Collections.sort(pitchers);
    }
    
    public void removePitcher (Pitcher p) {
        pitchers.remove(p);
    }
    
    public ObservableList<Player> getPlayers() {
        return players;
    }
    
    public void addPlayer (Player p) {
        players.add(p);
        Collections.sort(players);
    }
    
    public void removePlayer (Player p) {
        players.remove(p);
    }
    
    public void clearDataList() {
        players.removeAll(players);
        hitters.removeAll(hitters);
        pitchers.removeAll(pitchers);
    }
    
    public void clearPlayers() {
        players.clear();
    }
    
    public void clearPitchers() {
        pitchers.clear();
    }
    
    public void clearHitters() {
        hitters.clear();
    }
    
    public ObservableList<Integer> getStatsR() {
        return statsR;
    }
    
    public ObservableList<Integer> getStatsHR() {
        return statsHR;
    }
    
    public ObservableList<Integer> getStatsRBI() {
        return statsRBI;
    }
    
    public ObservableList<Integer> getStatsSB() {
        return statsSB;
    }
    
    public ObservableList<Double> getStatsBA() {
        return statsBA;
    }
    
    public ObservableList<Integer> getStatsW() {
        return statsW;
    }
    
    public ObservableList<Integer> getStatsSV() {
        return statsSV;
    }
    
    public ObservableList<Integer> getStatsK() {
        return statsK;
    }
    
    public ObservableList<Double> getStatsERA() {
        return statsERA;
    }
    
    public ObservableList<Double> getStatsWHIP() {
        return statsWHIP;
    }
    
    public void calcRankings() {
        
        for (int i = 0; i < pitchers.size(); i++) {
            statsW.add(pitchers.get(i).getR_w());
            statsSV.add(pitchers.get(i).getHr_sv());
            statsK.add(pitchers.get(i).getRbi_k());
            statsERA.add(pitchers.get(i).getSb_era());
            statsWHIP.add(pitchers.get(i).getBa_whip());
        }
        
        for (int i = 0; i < hitters.size(); i++) {
            statsR.add(hitters.get(i).getR_w());
            statsHR.add(hitters.get(i).getHr_sv());
            statsRBI.add(hitters.get(i).getRbi_k());
            statsSB.add(hitters.get(i).getStolenBases());
            statsBA.add(hitters.get(i).getBa_whip());
        }
        
        Collections.sort(statsW);
        Collections.sort(statsSV);
        Collections.sort(statsK);
        Collections.sort(statsERA);
        Collections.sort(statsWHIP);
        Collections.sort(statsR);
        Collections.sort(statsHR);
        Collections.sort(statsRBI);
        Collections.sort(statsSB);
        Collections.sort(statsBA);
        
        Collections.reverse(statsW);
        Collections.reverse(statsSV);
        Collections.reverse(statsK);
//        Collections.reverse(statsERA);
//        Collections.reverse(statsWHIP);
        Collections.reverse(statsR);
        Collections.reverse(statsHR);
        Collections.reverse(statsRBI);
        Collections.reverse(statsSB);
        Collections.reverse(statsBA);
        
        for (int i = 0; i < pitchers.size(); i++) {
            
            for (int j = 0; j < statsW.size(); j++) {
                if (pitchers.get(i).getR_w() == statsW.get(j)) {
                    pitchers.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsSV.size(); j++) {
                if (pitchers.get(i).getHr_sv()== statsSV.get(j)) {
                    pitchers.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsK.size(); j++) {
                if (pitchers.get(i).getRbi_k() == statsK.get(j)) {
                    pitchers.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsERA.size(); j++) {
                if (pitchers.get(i).getSb_era() == statsERA.get(j)) {
                    pitchers.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsWHIP.size(); j++) {
                if (pitchers.get(i).getBa_whip() == statsWHIP.get(j)) {
                    pitchers.get(i).getRankings().add(j);
                    break;
                }
            }
        }
        
        for (int i = 0; i < hitters.size(); i++) {
            
            for (int j = 0; j < statsR.size(); j++) {
                if (hitters.get(i).getR_w() == statsR.get(j)) {
                    hitters.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsHR.size(); j++) {
                if (hitters.get(i).getHr_sv() == statsHR.get(j)) {
                    hitters.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsRBI.size(); j++) {
                if (hitters.get(i).getRbi_k()== statsRBI.get(j)) {
                    hitters.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsSB.size(); j++) {
                if (hitters.get(i).getSb_era() == statsSB.get(j)) {
                    hitters.get(i).getRankings().add(j);
                    break;
                }
            }
            
            for (int j = 0; j < statsBA.size(); j++) {
                if (hitters.get(i).getBa_whip() == statsBA.get(j)) {
                    hitters.get(i).getRankings().add(j);
                    break;
                }
            }
        }
    }
    
    public void clearRankings() {
        statsR.clear();
        statsHR.clear();
        statsRBI.clear();
        statsSB.clear();
        statsBA.clear();
        statsW.clear();
        statsSV.clear();
        statsK.clear();
        statsERA.clear();
        statsWHIP.clear();
        
        for (int i = 0; i < players.size(); i++) {
            players.get(i).getRankings().clear();
        }
    }
    
    public void setEstimatedValues(TeamHolder tH) {
        
        calcRankings();
        
        for (int i = 0; i < pitchers.size(); i++) {
            pitchersRankings.add(pitchers.get(i).calcTotalPoints());
        }
        
        for (int i = 0; i < hitters.size(); i++) {
            hittersRankings.add(hitters.get(i).calcTotalPoints());
        }
        
        Collections.sort(pitchersRankings);
        Collections.sort(hittersRankings);
        
        for (int i = 0; i < pitchersRankings.size(); i++) {
            
            for (int j = 0; j < pitchers.size(); j++) {
                if (pitchersRankings.get(i) == pitchers.get(j).calcTotalPoints()) {
                    pitchers.get(j).setPlayerRanking(i + 1);
                }
            }
        }
        
        for (int i = 0; i < hittersRankings.size(); i++) {
            
            for (int j = 0; j < hitters.size(); j++) {
                if (hittersRankings.get(i) == hitters.get(j).calcTotalPoints()) {
                    hitters.get(j).setPlayerRanking(i + 1);
                }
            }
        }
        
        int x = tH.getHittersRequired();
        int y = tH.getPitchersRequired();
        double mediumSalaryHitter;
        double mediumSalaryPitcher;
        
        if (x == 0 || y == 0) {
            mediumSalaryHitter = tH.getAllBudget() / ((2*x) + 1);
            mediumSalaryPitcher = tH.getAllBudget() / ((2*y) + 1);
        }
        else {
            mediumSalaryHitter = tH.getAllBudget() / (2*x);
            mediumSalaryPitcher = tH.getAllBudget() / (2*y);  
        }
        
        for (int i = 0; i < hitters.size(); i++) {
            hitters.get(i).setEstimatedValue((int)((mediumSalaryHitter * (((double)x * 2) / hitters.get(i).getPlayerRanking())) + 1));
        }
        
        for (int i = 0; i < pitchers.size(); i++) {
            pitchers.get(i).setEstimatedValue((int)((mediumSalaryPitcher * (((double)y * 2) / pitchers.get(i).getPlayerRanking())) + 1));
        }
        
    }

}
