/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 *
 * @author Kevin
 */
public class TeamHolder {
    
    ObservableList<FantasyTeam> teams;
    
    ObservableList<Integer> statsR;
    ObservableList<Integer> statsHR;
    ObservableList<Integer> statsRBI;
    ObservableList<Integer> statsSB;
    ObservableList<Double> statsBA;
    ObservableList<Integer> statsW;
    ObservableList<Integer> statsSV;
    ObservableList<Integer> statsK;
    ObservableList<Double> statsERA;
    ObservableList<Double> statsWHIP;
    
    public TeamHolder() {
        
        teams = FXCollections.observableArrayList();
        
        statsR = FXCollections.observableArrayList();
        statsHR = FXCollections.observableArrayList();
        statsRBI = FXCollections.observableArrayList();
        statsSB = FXCollections.observableArrayList();
        statsBA = FXCollections.observableArrayList();
        statsW = FXCollections.observableArrayList();
        statsSV = FXCollections.observableArrayList();
        statsK = FXCollections.observableArrayList();
        statsERA = FXCollections.observableArrayList();
        statsWHIP = FXCollections.observableArrayList();
    }
    
    public ObservableList<FantasyTeam> getTeams() {
        return teams;
    }
    
    public void addTeam (FantasyTeam ft) {
        teams.add(ft);
        Collections.sort(teams);
    }
    
    public void removeTeam (FantasyTeam ft) {
        teams.remove(ft);
    }
    
    public void clearTeams() {
        teams.clear();
    }
    
    public ObservableList<Integer> getStatsR() {
        return statsR;
    }
    
    public ObservableList<Integer> getStatsHR() {
        return statsHR;
    }
    
    public ObservableList<Integer> getStatsRBI() {
        return statsRBI;
    }
    
    public ObservableList<Integer> getStatsSB() {
        return statsSB;
    }
    
    public ObservableList<Double> getStatsBA() {
        return statsBA;
    }
    
    public ObservableList<Integer> getStatsW() {
        return statsW;
    }
    
    public ObservableList<Integer> getStatsSV() {
        return statsSV;
    }
    
    public ObservableList<Integer> getStatsK() {
        return statsK;
    }
    
    public ObservableList<Double> getStatsERA() {
        return statsERA;
    }
    
    public ObservableList<Double> getStatsWHIP() {
        return statsWHIP;
    }
    
    public void clearRankings() {
        statsR.clear();
        statsHR.clear();
        statsRBI.clear();
        statsSB.clear();
        statsBA.clear();
        statsW.clear();
        statsSV.clear();
        statsK.clear();
        statsERA.clear();
        statsWHIP.clear();
        
        for (int i = 0; i < teams.size(); i++) {
            teams.get(i).getRankings().clear();
        }
    }
    
    public void calcRankings() {
        
        // FILL IN EACH STAT LIST
        for (int i = 0; i < teams.size(); i++) {
            statsR.add(teams.get(i).getStandingsR());
            statsHR.add(teams.get(i).getStandingsHR());
            statsRBI.add(teams.get(i).getStandingsRBI());
            statsSB.add(teams.get(i).getStandingsSB());
            statsBA.add(teams.get(i).getStandingsBA());
            statsW.add(teams.get(i).getStandingsW());
            statsSV.add(teams.get(i).getStandingsSV());
            statsK.add(teams.get(i).getStandingsK());
            statsERA.add(teams.get(i).getStandingsERA());
            statsWHIP.add(teams.get(i).getStandingsWHIP());
        }
        
        // SORTS ALL LISTS IN DESCENDING ORDER
        Collections.sort(statsR);
        Collections.reverse(statsR);
        
        Collections.sort(statsHR);
        Collections.reverse(statsHR);
        
        Collections.sort(statsRBI);
        Collections.reverse(statsRBI);
        
        Collections.sort(statsSB);
        Collections.reverse(statsSB);
        
        Collections.sort(statsBA);
        Collections.reverse(statsBA);
        
        Collections.sort(statsW);
        Collections.reverse(statsW);
        
        Collections.sort(statsSV);
        Collections.reverse(statsSV);
        
        Collections.sort(statsK);
        Collections.reverse(statsK);
        
        Collections.sort(statsERA);
//        Collections.reverse(statsERA);
        
        Collections.sort(statsWHIP);
//        Collections.reverse(statsWHIP);
        
        // SET THE RANKINGS FOR EACH TEAM
        for (int i = 0; i < teams.size(); i++) {
            
            for (int j = 0; j < statsR.size(); j++) {
                if (teams.get(i).getStandingsR() == statsR.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j); // REPLACE WITH TEAMS.SIZE() - J???
                    break;
                }
            }
            
            for (int j = 0; j < statsHR.size(); j++) {
                if (teams.get(i).getStandingsHR() == statsHR.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            for (int j = 0; j < statsRBI.size(); j++) {
                if (teams.get(i).getStandingsRBI() == statsRBI.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            for (int j = 0; j < statsSB.size(); j++) {
                if (teams.get(i).getStandingsSB() == statsSB.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            for (int j = 0; j < statsBA.size(); j++) {
                if (teams.get(i).getStandingsBA() == statsBA.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            for (int j = 0; j < statsW.size(); j++) {
                if (teams.get(i).getStandingsW() == statsW.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            //THIS GETS RUN TWICE... WHY??? --> BECAUSE BOTH TEAMS HAVE 0 SV
            for (int j = 0; j < statsSV.size(); j++) {
                if (teams.get(i).getStandingsSV() == statsSV.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            for (int j = 0; j < statsK.size(); j++) {
                if (teams.get(i).getStandingsK() == statsK.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            for (int j = 0; j < statsERA.size(); j++) {
                if (teams.get(i).getStandingsERA() == statsERA.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
            
            for (int j = 0; j < statsWHIP.size(); j++) {
                if (teams.get(i).getStandingsWHIP() == statsWHIP.get(j)) {
                    teams.get(i).getRankings().add(teams.size() - j);
                    break;
                }
            }
        }
    }
    
    public int getPitchersRequired() {
        int n = 0;
        
        for (int i = 0; i < teams.size(); i++) {
            n+= teams.get(i).getCurrentQuota().get("P");
        }
        
        return (teams.size() * 9) - n;
    }
    
    public int getHittersRequired() {
        int n = 0;
                
        for (int i = 0; i < teams.size(); i++) {
            n+= teams.get(i).getListOfActiveHitters().size();
        }
        
        return (teams.size() * 14) - n;
    }
    
    public int getTaxiRequired() {
        int n = 0;
        
        for (int i = 0; i < teams.size(); i++) {
            n+= teams.get(i).getListOfTaxiPlayers().size();
        }
        
        return (teams.size() * 8) - n;
    }
    
    public int getAllBudget() {
        int n = 0;
        
        for (int i = 0; i < teams.size(); i++) {
            n+= teams.get(i).getTotalBudget();
        }
        
        return n;
    }
}
