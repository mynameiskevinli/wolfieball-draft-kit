/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.io.IOException;
import wbdk.file.FileManager;
import java.time.DayOfWeek;
import java.time.LocalDate;
import static wbdk.WBDK_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wbdk.WBDK_StartupConstants.JSON_FILE_PATH_PITCHERS;
import wbdk.file.JsonFileManager;

/**
 *
 * @author Kevin
 */
public class DataManager {
    
//    PlayerHolder playerHolder;
//    TeamHolder teamHolder;
    DataView view;
    FileManager fileManager;
    Draft draft;
    JsonFileManager jsonFileManager;
    
    
    public DataManager(DataView initView) {
        view = initView;
        draft = new Draft();
        jsonFileManager = new JsonFileManager();
//        playerHolder = phToLoad;
//        teamHolder = new TeamHolder();
    }
    
    public JsonFileManager getJSONFileManager() {
        return jsonFileManager;
    }
    
    public void setJSONFileManager(JsonFileManager jsm) {
        jsonFileManager = jsm;
    }
    
    public Draft getDraft() {
        return draft;
    }
    
    public void setDraft(Draft draftToSet) {
        draft = draftToSet;
    }
    
    public PlayerHolder getPlayerHolder() {
        return draft.getPlayerHolder();
    }
    
    public TeamHolder getTeamHolder() {
        return draft.getTeamHolder();
    }
    
    public FileManager getFileManager() {
        return fileManager;
    }
    
    public void reset() {
        draft.setDraftName("");
        draft.getPlayerHolder().clearDataList();
        draft.getTeamHolder().clearTeams();
        try {
            draft.setPlayerHolder(jsonFileManager.loadPlayerHolder(JSON_FILE_PATH_PITCHERS, JSON_FILE_PATH_HITTERS));
        } catch (IOException ioe) {
            
        }
        
        view.reloadDraft(draft);
    }
}
