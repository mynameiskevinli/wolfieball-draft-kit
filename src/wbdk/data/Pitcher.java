/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.text.DecimalFormat;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 *
 * @author Kevin
 */
public class Pitcher extends Player{
    
    final IntegerProperty basesOnBall;
    final IntegerProperty earnedRuns;
    final IntegerProperty hits;
    final DoubleProperty inningsPitched;
    final IntegerProperty strikeouts;
    final IntegerProperty saves;
    final IntegerProperty wins;
    final IntegerProperty walks;
    final DoubleProperty ERA;
    final DoubleProperty WHIP;
    final StringProperty position;
    
    final IntegerProperty r_w;
    final IntegerProperty hr_sv;
    final IntegerProperty rbi_k;
    final DoubleProperty sb_era;
    final DoubleProperty ba_whip;
    
    final StringProperty pos;
    
    public Pitcher() {
        basesOnBall = new SimpleIntegerProperty(0);
        earnedRuns = new SimpleIntegerProperty(0);
        hits = new SimpleIntegerProperty(0);
        inningsPitched = new SimpleDoubleProperty(0);
        strikeouts = new SimpleIntegerProperty(0);
        saves = new SimpleIntegerProperty(0);
        wins = new SimpleIntegerProperty(0);
        walks = new SimpleIntegerProperty(0);
        ERA = new SimpleDoubleProperty(0);
        WHIP = new SimpleDoubleProperty(0);
        position = new SimpleStringProperty("");
        
        r_w = new SimpleIntegerProperty(0);
        hr_sv = new SimpleIntegerProperty(0);
        rbi_k = new SimpleIntegerProperty(0);
        sb_era = new SimpleDoubleProperty(0);
        ba_whip = new SimpleDoubleProperty(0);
        
        pos = new SimpleStringProperty("");
    }
    
    public void setBasesOnBall(int info) {
        basesOnBall.set(info);
    }
    
    public int getBasesOnBall() {
        return basesOnBall.get();
    }
    
    public void setEarnedRuns(int info) {
        earnedRuns.set(info);
    }
    
    public int getEarnedRuns() {
        return earnedRuns.get();
    }
    
    public void setHits(int info) {
        hits.set(info);
    }
    
    public int getHits() {
        return hits.get();
    }
    
    public void setInningsPitched(double info) {
        inningsPitched.set(info);
    }
    
    public double getInningsPitched() { 
        return inningsPitched.get();
    }
    
    public void setStrikeouts(int info) {
        strikeouts.set(info);
        rbi_k.set(info);
    }
    
    public int getStrikeouts() {
        return strikeouts.get();
    }
    
    public void setSaves(int info) {
        saves.set(info);
        hr_sv.set(info);
    }
    
    public int getSaves() {
        return saves.get();
    }
    
    public void setWins(int info) {
        wins.set(info);
        r_w.set(info);
    }
    
    public int getWins() {
        return wins.get();
    }
    
    public void setWalks(int info) {
        walks.set(info);
    }
    
    public int getWalks() {
        return walks.get();
    }
    
    public void setPosition(String info) {
        position.set(info);
        pos.set(info);
    }
    
    public String getPosition() {
        return position.get();
    }
    
    public void calculateERA(int earnedRuns, double inningsPitched) {
        if (inningsPitched != 0) {
            double result = ((double)earnedRuns)*9/inningsPitched;
            DecimalFormat numberFormat = new DecimalFormat("#.00");
            ERA.set(Double.parseDouble(numberFormat.format(result)));
            sb_era.set(Double.parseDouble(numberFormat.format(result)));
        }
    }
    
    public void calculateWHIP(int walks, int hits, double inningsPitched) {
        if (inningsPitched != 0) {
            double result = ((double)walks)+hits/inningsPitched;
            DecimalFormat numberFormat = new DecimalFormat("#.00");
            WHIP.set(Double.parseDouble(numberFormat.format(result)));
            ba_whip.set(Double.parseDouble(numberFormat.format(result)));
        }
    }
    
    public int getR_w() {
        return r_w.get();
    }
    
    public int getHr_sv() {
        return hr_sv.get();
    }
    
    public int getRbi_k() {
        return rbi_k.get();
    }
    
    public double getSb_era() {
        return sb_era.get();
    }
    
    public double getBa_whip() {
        return ba_whip.get();
    }
    
    public String getPos() {
        return pos.get();
    }
    
//    public int calcTotalPoints() {
//        int n = 0;
//        for (int i = 0; i < rankings.size(); i++) {
//            n+= rankings.get(i);
////            System.out.println("RANKING POINTS" + i + ": " + rankings.get(i));
//        }
////        System.out.println("");
////        System.out.println("");
//        return n;
//    }
}
