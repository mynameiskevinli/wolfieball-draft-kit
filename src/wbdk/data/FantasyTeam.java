/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import java.util.Map;
import java.util.HashMap;
import java.util.Stack;

/**
 *
 * @author Kevin
 */
public class FantasyTeam implements Comparable {
    Map<String,Integer> maxQuota;
    Map<String,Integer> currentQuota;
    
    ObservableList<Player> listOfActivePlayers;
    ObservableList<Player> listOfTaxiPlayers;
    
    ObservableList<Pitcher> listOfActivePitchers;
    ObservableList<Hitter> listOfActiveHitters;
    
    ObservableList<Integer> rankings;
    
    final StringProperty teamName;
    final StringProperty ownerName;
    final IntegerProperty playersNeeded;
    final IntegerProperty totalBudget;
    final IntegerProperty pricePerPlayer;
    final IntegerProperty standingsR;
    final IntegerProperty standingsHR;
    final IntegerProperty standingsRBI;
    final IntegerProperty standingsSB;
    final DoubleProperty standingsBA;
    final IntegerProperty standingsW;
    final IntegerProperty standingsSV;
    final IntegerProperty standingsK;
    final DoubleProperty standingsERA;
    final DoubleProperty standingsWHIP;
    final IntegerProperty totalPoints;
    
    int maxC = 2;
    int max1B = 1;
    int maxCI = 1;
    int max3B = 1;
    int max2B = 1;
    int maxMI = 1;
    int maxSS = 1;
    int maxU = 1;
    int maxOF = 5;
    int maxP = 9;
    
    public FantasyTeam() {
        
        maxQuota = new HashMap<String,Integer>();
        currentQuota = new HashMap<String,Integer>();
    
        maxQuota.put("C", maxC);
        maxQuota.put("1B", max1B);
        maxQuota.put("CI", maxCI);
        maxQuota.put("3B", max3B);
        maxQuota.put("2B", max2B);
        maxQuota.put("MI", maxMI);
        maxQuota.put("SS", maxSS);
        maxQuota.put("U", maxU);
        maxQuota.put("OF", maxOF);
        maxQuota.put("P", maxP);
        
        currentQuota.put("C", 0);
        currentQuota.put("1B", 0);
        currentQuota.put("CI", 0);
        currentQuota.put("3B", 0);
        currentQuota.put("2B", 0);
        currentQuota.put("MI", 0);
        currentQuota.put("SS", 0);
        currentQuota.put("U", 0);
        currentQuota.put("OF", 0);
        currentQuota.put("P", 0);
        
        listOfActivePlayers = FXCollections.observableArrayList();
        listOfTaxiPlayers = FXCollections.observableArrayList();

        listOfActivePitchers = FXCollections.observableArrayList();
        listOfActiveHitters = FXCollections.observableArrayList();
        
        rankings = FXCollections.observableArrayList();
        
        teamName = new SimpleStringProperty("");
        ownerName = new SimpleStringProperty("");
        playersNeeded = new SimpleIntegerProperty(23);
        totalBudget = new SimpleIntegerProperty(260);
        pricePerPlayer = new SimpleIntegerProperty(11);
        standingsR = new SimpleIntegerProperty(0);
        standingsHR = new SimpleIntegerProperty(0);
        standingsRBI = new SimpleIntegerProperty(0);
        standingsSB = new SimpleIntegerProperty(0);
        standingsBA = new SimpleDoubleProperty(0);
        standingsW = new SimpleIntegerProperty(0);
        standingsSV = new SimpleIntegerProperty(0);
        standingsK = new SimpleIntegerProperty(0);
        standingsERA = new SimpleDoubleProperty(0);
        standingsWHIP = new SimpleDoubleProperty(0);
        totalPoints = new SimpleIntegerProperty(0);
    }
    
    public void addActivePlayer(Player player) {
        listOfActivePlayers.add(player);
    }
    
    public void removeActivePlayer(Player player) {
        listOfActivePlayers.remove(player);
    }
    
    public void addActivePitcher(Pitcher pitcher) {
        listOfActivePitchers.add(pitcher);
    }
    
    public void removeActivePitcher(Pitcher pitcher) {
        listOfActivePitchers.remove(pitcher);
    }
    
    public void addActiveHitter(Hitter hitter) {
        listOfActiveHitters.add(hitter);
    }
    
    public void removeActiveHitter(Hitter hitter) {
        listOfActiveHitters.remove(hitter);
    }
    
    public ObservableList<Player> getListOfActivePlayers() {
        return listOfActivePlayers;
    }
    
    public ObservableList<Pitcher> getListOfActivePitchers() {
        return listOfActivePitchers;
    }
    
    public ObservableList<Hitter> getListOfActiveHitters() {
        return listOfActiveHitters;
    }
    
    public ObservableList<Integer> getRankings() {
        return rankings;
    }
        
    public void addTaxiPlayer(Player player) {
        listOfTaxiPlayers.add(player);
    }
    
    public void removeTaxiPlayer(Player player) {
        listOfTaxiPlayers.remove(player);
    }
    
    public ObservableList<Player> getListOfTaxiPlayers() {
        return listOfTaxiPlayers;
    }
    
    public String getTeamName() {
        return teamName.get();
    }
    
    public void setTeamName(String info) {
        teamName.set(info);
    }
    
    public String getTeamOwner() {
        return ownerName.get();
    }
    
    public void setTeamOwner(String info) {
        ownerName.set(info);
    }
    
    @Override
    public int compareTo(Object obj) {
        FantasyTeam otherTeam = (FantasyTeam)obj;
        return getTeamName().compareTo(otherTeam.getTeamName());
    }
    
    public Map<String, Integer> getCurrentQuota() {
        return currentQuota;
    }
    
    public void setCurrentQuota(Map<String, Integer> quota) {
        currentQuota = quota;
    }
    
    public Map<String, Integer> getMaxQuota() {
        return maxQuota;
    }
    
    public void sortTeamPlayers() {
        Stack<Player> sortStack = new Stack<Player>();
        Stack<Player> reverseStack = new Stack<Player>();
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < listOfActivePlayers.size(); j++) {
                if (listOfActivePlayers.get(j).getValue() == i) {
                    sortStack.push(listOfActivePlayers.get(j));
                }
            }
        }
        
        while (sortStack.size() != 0) {
            reverseStack.push(sortStack.pop());
        }
        
        listOfActivePlayers.clear();
        
        while (reverseStack.size() != 0) {
            listOfActivePlayers.add(reverseStack.pop());
        }
    }
    
    public int getPlayersNeeded() {
        return playersNeeded.get();
    }
    
    public void setPlayersNeeded(int info) {
        playersNeeded.set(info);
    }
    
    
    public int getTotalBudget() {
        return totalBudget.get();
    }
    
    public void setTotalBudget(int info) {
        totalBudget.set(info);
    }
    
    public int getPricePerPlayer() {
        return pricePerPlayer.get();
    }
    
    public void setPricePerPlayer(int info) {
        pricePerPlayer.set(info);
    }
    
    public int getStandingsR() {
        return standingsR.get();
    }
    
    public void setStandingsR(int info) {
        standingsR.set(info);
    }
    
    public int getStandingsHR() {
        return standingsHR.get();
    }
    
    public void setStandingsHR(int info) {
        standingsHR.set(info);
    }
    
    public int getStandingsRBI() {
        return standingsRBI.get();
    }
    
    public void setStandingsRBI(int info) {
        standingsRBI.set(info);
    }
    
    public int getStandingsSB() {
        return standingsSB.get();
    }
    
    public void setStandingsSB(int info) {
        standingsSB.set(info);
    }
    
    public double getStandingsBA() {
        return standingsBA.get();
    }
    
    public void setStandingsBA(double info) {
        standingsBA.set(info);
    }
    
    public int getStandingsW() {
        return standingsW.get();
    }
    
    public void setStandingsW(int info) {
        standingsW.set(info);
    }
    
    public int getStandingsSV() {
        return standingsSV.get();
    }
    
    public void setStandingsSV(int info) {
        standingsSV.set(info);
    }
    
    public int getStandingsK() {
        return standingsK.get();
    }
    
    public void setStandingsK(int info) {
        standingsK.set(info);
    }
    
    public double getStandingsERA() {
        return standingsERA.get();
    }
    
    public void setStandingsERA(double info) {
        standingsERA.set(info);
    }
    
    public double getStandingsWHIP() {
        return standingsWHIP.get();
    }
    
    public void setStandingsWHIP(double info) {
        standingsWHIP.set(info);
    }

    public int getTotalPoints() {
        return totalPoints.get();
    }
    
    public void setTotalPoints(int info) {
        totalPoints.set(info);
    }
    
    //THIS UPDATES TABLE IMMEDIATELY AFTER EDITTING
    public IntegerProperty playersNeededProperty() {
        return playersNeeded;
    }
    
    public IntegerProperty totalBudgetProperty() {
        return totalBudget;
    }
    
    public IntegerProperty pricePerPlayerProperty() {
        return pricePerPlayer;
    }
    
    public IntegerProperty standingsRProperty() {
        return standingsR;
    }
    
    public IntegerProperty standingsHRProperty() {
        return standingsHR;
    }
    
    public IntegerProperty standingsRBIProperty() {
        return standingsRBI;
    }
    
    public IntegerProperty standingsSBProperty() {
        return standingsSB;
    }
    
    public DoubleProperty standingsBAProperty() {
        return standingsBA;
    }
    
    public IntegerProperty standingsWProperty() {
        return standingsW;
    }
    
    public IntegerProperty standingsSVProperty() {
        return standingsSV;
    }
    
    public IntegerProperty standingsKProperty() {
        return standingsK;
    }
    
    public DoubleProperty standingsERAProperty() {
        return standingsERA;
    }
    
    public DoubleProperty standingsWHIPProperty() {
        return standingsWHIP;
    }
    
    public IntegerProperty totalPointsProperty() {
        return totalPoints;
    }
    
    public int calcPlayersNeeded() {
        return 23 - listOfActivePlayers.size();
    }
    
    public int calcTotalBudget() {
        int n = 0;
        for (int i = 0; i < listOfActivePlayers.size(); i++) {
            n+= listOfActivePlayers.get(i).getSalary();
        }
        return 260 - n;
    }
    
    public int calcPricePerPlayer() {
        if (calcPlayersNeeded() == 0) {
            return -1;
        }
        else {
            return calcTotalBudget()/calcPlayersNeeded();
        }
    }
    
    public int calcR() {
        int n = 0;
        for (int i = 0; i < listOfActiveHitters.size(); i++) {
            n+= listOfActiveHitters.get(i).getRuns();
        }
        return n;
    }
    
    public int calcHR() {
        int n = 0;
        for (int i = 0; i < listOfActiveHitters.size(); i++) {
            n+= listOfActiveHitters.get(i).getHomeruns();
        }
        return n;
    }
    
    public int calcRBI() {
        int n = 0;
        for (int i = 0; i < listOfActiveHitters.size(); i++) {
            n+= listOfActiveHitters.get(i).getRunsBattedIn();
        }
        return n;
    }
    
    public int calcSB() {
        int n = 0;
        for (int i = 0; i < listOfActiveHitters.size(); i++) {
            n+= listOfActiveHitters.get(i).getStolenBases();
        }
        return n;
    }
    
    public double calcBA() {
        double result;
        double n = 0;
        for (int i = 0; i < listOfActiveHitters.size(); i++) {
            Hitter hitter = listOfActiveHitters.get(i);
            n+= hitter.getBa_whip();
        }
        try {
            result =  n/listOfActiveHitters.size(); 
            DecimalFormat numberFormat = new DecimalFormat("#.000");
            return (Double.parseDouble(numberFormat.format(result)));
        } catch (Exception e) {
            return 0;
        }
    }
    
    public int calcW() {
        int n = 0;
        for (int i = 0; i < listOfActivePitchers.size(); i++) {
            Pitcher pitcher = listOfActivePitchers.get(i);
            n+= pitcher.getWins();
        }
        return n;
    }
    
    public int calcSV() {
        int n = 0;
        for (int i = 0; i < listOfActivePitchers.size(); i++) {
            Pitcher pitcher = listOfActivePitchers.get(i);
            n+= pitcher.getSaves();
        }
        return n;
    }
    
    public int calcK() {
        int n = 0;
        for (int i = 0; i < listOfActivePitchers.size(); i++) {
            Pitcher pitcher = listOfActivePitchers.get(i);
            n+= pitcher.getStrikeouts();
        }
        return n;
    }
    
    public double calcERA() {
        double result;
        double n = 0;
        for (int i = 0; i < listOfActivePitchers.size(); i++) {
            Pitcher pitcher = listOfActivePitchers.get(i);
            n+= pitcher.getSb_era();
        }
        try {
            result = n/listOfActivePitchers.size(); 
            DecimalFormat numberFormat = new DecimalFormat("#.00");
            return (Double.parseDouble(numberFormat.format(result)));
        } catch (Exception e) {
            return 0;
        }

    }
    
    public double calcWHIP() {
        double result;
        double n = 0;
        for (int i = 0; i < listOfActivePitchers.size(); i++) {
            Pitcher pitcher = listOfActivePitchers.get(i);
            n+= pitcher.getBa_whip();
        }
        try {
            result = n/listOfActivePitchers.size(); 
            DecimalFormat numberFormat = new DecimalFormat("#.00");
            return (Double.parseDouble(numberFormat.format(result)));
        } catch (Exception e) {
            return 0;
        }
        
    }
    
    public int calcTotalPoints() {
        int n = 0;
        for (int i = 0; i < rankings.size(); i++) {
            n+= rankings.get(i);
//            System.out.println("RANKING POINTS" + i + ": " + rankings.get(i));
        }
        System.out.println("");
        System.out.println("");
        return n;
    }
    
}
