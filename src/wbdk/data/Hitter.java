/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;

/**
 *
 * @author Kevin
 */
public class Hitter extends Player{
    
    final StringProperty qualifiedPosition;
    final IntegerProperty runs;
    final IntegerProperty hits;
    final IntegerProperty homeruns;
    final IntegerProperty runsBattedIn;
    final IntegerProperty stolenBases;
    final IntegerProperty atBats;
    final DoubleProperty battingAverage;
    
    final IntegerProperty r_w;
    final IntegerProperty hr_sv;
    final IntegerProperty rbi_k;
    final DoubleProperty sb_era;
    final DoubleProperty ba_whip;

    final StringProperty pos;

    public Hitter() {
        qualifiedPosition = new SimpleStringProperty("");
        runs = new SimpleIntegerProperty(0);
        hits = new SimpleIntegerProperty(0);
        homeruns = new SimpleIntegerProperty(0);
        runsBattedIn = new SimpleIntegerProperty(0);
        stolenBases = new SimpleIntegerProperty(0);
        atBats = new SimpleIntegerProperty(0);
        battingAverage = new SimpleDoubleProperty(0);
        
        r_w = new SimpleIntegerProperty(0);
        hr_sv = new SimpleIntegerProperty(0);
        rbi_k = new SimpleIntegerProperty(0);
        sb_era = new SimpleDoubleProperty(0);
        ba_whip = new SimpleDoubleProperty(0);
        
        pos = new SimpleStringProperty("");
    }
    
    public void setRuns(int info) {
        runs.set(info);
        r_w.set(info);
    }
    
    public int getRuns() {
        return runs.get();
    }
    
    public void setHits(int info) {
        hits.set(info);
    }
    
    public int getHits() {
        return hits.get();
    }
    
    public void setHomeruns(int info) {
        homeruns.set(info);
        hr_sv.set(info);
    }
    
    public int getHomeruns() {
        return homeruns.get();
    }
    
    public void setRunsBattedIn(int info) {
        runsBattedIn.set(info);
        rbi_k.set(info);
    }
    
    public int getRunsBattedIn() { 
        return runsBattedIn.get();
    }
    
    public void setStolenBases(int info) {
        stolenBases.set(info);
        sb_era.set(info);
    }
    
    public int getStolenBases() {
        return stolenBases.get();
    }
    
    public void setAtBats(int info) {
        atBats.set(info);
    }
    
    public int getAtBats() {
        return atBats.get();
    }
    
    public void setQualifiedPosition(String info) {
        qualifiedPosition.set(info);
        pos.set(info);
    }
    
    public String getQualifiedPosition() {
        return qualifiedPosition.get();
    }
    
    public String[] getQPList() {
        String[] qpList;
        qpList = qualifiedPosition.get().split("_");
        return qpList;
    }
    
    public void calculateBA(int hits, int atBats) {
        if (atBats != 0) {
            double result = (double)hits/atBats;
            DecimalFormat numberFormat = new DecimalFormat("#.000");
            battingAverage.set(Double.parseDouble(numberFormat.format(result)));
            ba_whip.set(Double.parseDouble(numberFormat.format(result)));
        }
        
    }
    
    public int getR_w() {
        return r_w.get();
    }
    
    public int getHr_sv() {
        return hr_sv.get();
    }
    
    public int getRbi_k() {
        return rbi_k.get();
    }
    
    public double getSb_era() {
        return sb_era.get();
    }
    
    public double getBa_whip() {
        return ba_whip.get();
    }
    
    public String getPos() {
        return pos.get();
    }
    
//    public int calcTotalPoints() {
//        int n = 0;
//        for (int i = 0; i < rankings.size(); i++) {
//            n+= rankings.get(i);
////            System.out.println("RANKING POINTS" + i + ": " + rankings.get(i));
//        }
////        System.out.println("");
////        System.out.println("");
//        return n;
//    }
    
}
