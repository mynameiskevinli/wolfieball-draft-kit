/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

/**
 *
 * @author Kevin
 */
public class Draft {
    //FXCollections.ObservableList(ArrayList) is how to create observable lists
//    ArrayList<FantasyTeam> listOfFantasyTeams;
//    ArrayList<Player> listOfDraftPlayers;
    
    TeamHolder tH;
    PlayerHolder pH;
    
    String draftName;
    
    public Draft() {
        tH = new TeamHolder();
        pH = new PlayerHolder();
        draftName = "";
    }
    
    public TeamHolder getTeamHolder() {
        return tH;
    }
    
    public PlayerHolder getPlayerHolder() {
        return pH;
    }
    
    public void setTeamHolder(TeamHolder teamHolderToSet) {
        tH = teamHolderToSet;
    }
    
    public void setPlayerHolder(PlayerHolder playerHolderToSet) {
        pH = playerHolderToSet;
    }
    
    public String getDraftName() {
        return draftName;
    }
    
    public void setDraftName(String name) {
        draftName = name;
    }
}
