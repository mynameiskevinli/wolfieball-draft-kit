/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Kevin
 */
public class Player implements Comparable {

//    boolean isActive;
    boolean isHitter;
    final StringProperty playingPosition;
    final StringProperty fTeam;
    
    final StringProperty lastName;
    final StringProperty firstName;
    final StringProperty team;
    final IntegerProperty age;
    final IntegerProperty salary;
    final StringProperty contract;
    final StringProperty notes;
    final IntegerProperty yearOfBirth;
    final StringProperty nationOfBirth;
    final IntegerProperty estValue;
    final IntegerProperty pickNumber;
    
    ObservableList<Integer> rankings;
    
    int playerRanking;
    
    public Player() {
        
        lastName = new SimpleStringProperty("");
        firstName = new SimpleStringProperty("");
        team = new SimpleStringProperty("");
        age = new SimpleIntegerProperty(0);
        salary = new SimpleIntegerProperty(0);
        contract = new SimpleStringProperty("");
        notes = new SimpleStringProperty("");
        yearOfBirth = new SimpleIntegerProperty(0);
        nationOfBirth = new SimpleStringProperty("");
        isHitter = false;
        playingPosition = new SimpleStringProperty("");
        fTeam = new SimpleStringProperty("");
        estValue = new SimpleIntegerProperty();
        pickNumber = new SimpleIntegerProperty();
        
        rankings = FXCollections.observableArrayList();
    }
    
    public String getFirstName() {
        return firstName.get();
    }
    
    public void setFirstName(String info) {
        firstName.set(info);
    }
    
    public String getLastName() {
        return lastName.get();
    }
    
    public void setLastName(String info) {
        lastName.set(info);
    }
    
    public String getContract() {
        return contract.get();
    }
    
    public void setContract(String info) {
        contract.set(info);
    }
    
    public String getTeam() {
        return team.get();
    }
    
    public void setTeam(String info) {
        team.set(info);
    }
            
    public int getSalary() {
        return salary.get();
    }

    public void setSalary(int info) {
        salary.set(info);
    }
    
    public String getNotes() {
        return notes.get();
    }
    
    public void setNotes(String info) {
        notes.set(info);
    }
    
    //THIS UPDATES TABLE IMMEDIATELY AFTER EDITTING
    public StringProperty notesProperty() {
        return notes;
    }
    
    public int getYearOfBirth() {
        return yearOfBirth.get();
    }
        
    public void setYearOfBirth(int info) {
        yearOfBirth.set(info);
    }
    
    public String getNationOfBirth() {
        return nationOfBirth.get();
    }
        
    public void setNationOfBirth(String info) {
        nationOfBirth.set(info);
    }
    
    public void searchName(String name) {
        //Search by letter so search for both names... use contains?
    }
    
    public int getValue() {
        
        String pos = playingPosition.get();
        int value = -1;
        switch (pos) {
            case "C": value = 0; break;
            case "1B" : value = 1; break;
            case "CI" : value = 2; break;
            case "3B" : value = 3; break;
            case "2B" : value = 4; break;
            case "MI" : value = 5; break;
            case "SS" : value = 6; break;
            case "OF" : value = 7; break;
            case "U" : value = 8; break;
            case "P" : value = 9; break;
        }
        return value;
    }

    @Override
    public int compareTo(Object obj) {
        Player otherPlayer = (Player)obj;
        return getLastName().compareTo(otherPlayer.getLastName());
    }
    
    public void isHitter() {
        isHitter = true;
    }
    
    public boolean getHitter() {
        return isHitter;
    }
    
    public String getPlayingPosition() {
        return playingPosition.get();
    }
    
    public void setPlayingPosition(String info) {
        playingPosition.set(info);
    }
    
    public String getFTeam() {
        return fTeam.get();
    }
    
    public void setFTeam(String info) {
        fTeam.set(info);
    }
    
    public ObservableList<Integer> getRankings() {
        return rankings;
    }
    
    public int calcTotalPoints() {
        int n = 0;
        for (int i = 0; i < rankings.size(); i++) {
            n+= rankings.get(i);
//            System.out.println("RANKING POINTS" + i + ": " + rankings.get(i));
        }
//        System.out.println("");
//        System.out.println("");
        return n;
    }
    
    public void setPlayerRanking(int n) {
        playerRanking = n;
    }
    
    public int getPlayerRanking() {
        return playerRanking;
    }
    
    public int getEstimatedValue() {
        return estValue.get();
    }
        
    public void setEstimatedValue(int info) {
        estValue.set(info);
    }
    
    public int getPickNumber() {
        return pickNumber.get();
    }
        
    public void setPickNumber(int info) {
        pickNumber.set(info);
    }
}
